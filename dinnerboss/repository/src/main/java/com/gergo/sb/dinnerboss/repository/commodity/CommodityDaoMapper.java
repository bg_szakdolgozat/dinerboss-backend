package com.gergo.sb.dinnerboss.repository.commodity;

import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommodityDO;
import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommoditySearchParams;
import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CreateCommodityDO;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerSearchParams;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommodityDaoMapper {

    void create(CreateCommodityDO commodity);

    List<CommodityDO> page(@Param("page") PagedRequest page, @Param("param") CommoditySearchParams params);

    Long count(@Param("param") CommoditySearchParams params);

    void delete(@Param("id") Long id);

    void modify(@Param("commodity") CommodityDO remoteCommodity);

    void createTool(CreateCommodityDO tool);

    List<CommodityDO> pageTools(@Param("page") PagedRequest page, @Param("param") CommoditySearchParams params);

    Long countTools(@Param("param") CommoditySearchParams params);

    void deleteTool(@Param("id") Long id);
}
