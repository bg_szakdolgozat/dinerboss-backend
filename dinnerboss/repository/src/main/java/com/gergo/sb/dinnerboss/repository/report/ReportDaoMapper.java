package com.gergo.sb.dinnerboss.repository.report;

import com.gergo.sb.dinnerboss.serviceapi.service.checkout.CheckoutDO;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.report.CreateReportDO;
import com.gergo.sb.dinnerboss.serviceapi.service.report.ReportDO;
import com.gergo.sb.dinnerboss.serviceapi.service.report.ReportSearchParams;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ReportDaoMapper {

    void create(CreateReportDO report);

    List<ReportDO> page(@Param("page") PagedRequest page,@Param("param") ReportSearchParams params);

    Long count(@Param("param") ReportSearchParams params);
}
