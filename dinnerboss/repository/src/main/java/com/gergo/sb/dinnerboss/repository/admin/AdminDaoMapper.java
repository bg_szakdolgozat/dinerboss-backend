package com.gergo.sb.dinnerboss.repository.admin;


import com.gergo.sb.dinnerboss.serviceapi.service.admin.Admin;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.CreateAdminDO;

import java.util.Map;

public interface AdminDaoMapper {

    void create(CreateAdminDO createAdminDO);

    Admin get(Map<String, Object> params);
}
