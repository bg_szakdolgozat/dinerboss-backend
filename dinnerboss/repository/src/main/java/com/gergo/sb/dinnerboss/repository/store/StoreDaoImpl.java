package com.gergo.sb.dinnerboss.repository.store;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.store.CreateStoreDO;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreDO;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreSearchParams;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.store.StoreDao;
import org.springframework.util.Assert;

import java.util.List;

public class StoreDaoImpl implements StoreDao {

    private StoreDaoMapper storeDaoMapper;

    public StoreDaoImpl(StoreDaoMapper storeDaoMapper) {
        this.storeDaoMapper = storeDaoMapper;
    }

    @Override
    public void create(CreateStoreDO company) {
        storeDaoMapper.create(company);
    }

    @Override
    public StoreDO getById(Long id) {
        Assert.notNull(id, "id cannot be null!");
        var response = storeDaoMapper.getById(id);
        var companyId = storeDaoMapper.getCompanyIdByStore(id);
        response.setCompany(storeDaoMapper.getCompanyById(companyId));
        return response;
    }

    @Override
    public void delete(Long id) {
        Assert.notNull(id, "id cannot be null!");
        storeDaoMapper.delete(id);
    }

    @Override
    public void modify(StoreDO remoteStore) {
        storeDaoMapper.modify(remoteStore);

    }

    @Override
    public PagedData<StoreDO> page(PagedRequest page, StoreSearchParams params) {
        Assert.notNull(page, "StoreDaoImpl.page: page cannot be null!");
        Assert.notNull(page.getPageNumber(), "StoreDaoImpl.page: page.pageNumber cannot be null!");
        Assert.notNull(page.getPageSize(), "StoreDaoImpl.page: page.pageSize cannot be null!");
        var data = storeDaoMapper.page(page, params);
        data.forEach((a)->{
            var companyId = storeDaoMapper.getCompanyIdByStore(a.getId());
            a.setCompany(storeDaoMapper.getCompanyById(companyId));
        });
        var totalCount = storeDaoMapper.count(params);
        return new PagedData<>(data, totalCount);
    }

    @Override
    public List<StoreDO> getStoresByCompId(Long id) {
        Assert.notNull(id, "id cannot be null!");
        return storeDaoMapper.getStoresByCompId(id);
    }

    @Override
    public StoreDO getStoreByName(String name) {
        Assert.notNull(name, "id cannot be null!");
        var response = storeDaoMapper.getByName(name);
        return response;
    }

    @Override
    public void assignUserIds(Long id, List<Long> userIds) {
        var ids = storeDaoMapper.getUserIds(id);
        if(!ids.isEmpty()){
            storeDaoMapper.revokeUsersFromStore(id,ids);

        }

        Assert.notNull(userIds, "ids cannot be null!");
        storeDaoMapper.assignUsersToStore(id, userIds);
    }

    @Override
    public void revokeUserIds(Long storeId, List<Long> userIds) {
        Assert.notNull(userIds, "ids cannot be null!");
        storeDaoMapper.revokeUsersFromStore(storeId, userIds);
    }

    @Override
    public List<Long> getCustomerIds(Long storeId) {
        Assert.notNull(storeId, "storeId cannot be null!");
        return storeDaoMapper.getUserIds(storeId);
    }

    @Override
    public List<CustomerProfileDO> getCustomers(List<Long> customerIds) {
        Assert.notNull(customerIds, "customerIds cannot be null!");
        return storeDaoMapper.getCustomers(customerIds);
    }

}
