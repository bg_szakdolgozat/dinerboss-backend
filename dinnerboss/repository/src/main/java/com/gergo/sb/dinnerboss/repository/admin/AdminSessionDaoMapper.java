package com.gergo.sb.dinnerboss.repository.admin;

import com.gergo.sb.dinnerboss.serviceapi.service.admin.AdminSessionDO;
import org.apache.ibatis.annotations.Param;

import java.util.Optional;

public interface AdminSessionDaoMapper {
    void removeExpiredSessions();
    void insert(@Param("session") AdminSessionDO adminSession);
    Optional<AdminSessionDO> findBySessionId(@Param("sessionId") String sessionId);

}
