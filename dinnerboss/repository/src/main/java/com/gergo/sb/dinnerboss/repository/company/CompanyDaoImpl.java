package com.gergo.sb.dinnerboss.repository.company;

import com.gergo.sb.dinnerboss.repository.store.StoreDaoMapper;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.company.*;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.company.CompanyDao;
import org.springframework.util.Assert;

import java.lang.reflect.Array;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class CompanyDaoImpl implements CompanyDao {

    private CompanyDaoMapper companyDaoMapper;
    private StoreDaoMapper storeDaoMapper;


    public CompanyDaoImpl(CompanyDaoMapper companyDaoMapper, StoreDaoMapper storeDaoMapper) {
        this.companyDaoMapper = companyDaoMapper;
        this.storeDaoMapper = storeDaoMapper;
    }

    @Override
    public void create(CreateCompanyDO company) {
        companyDaoMapper.create(company);
    }

    @Override
    public CompanyDO getByName(String name) {
        Assert.notNull(name, "Name cannot be null!");
        Map<String, Object> params = Collections.singletonMap("name", name);
        return companyDaoMapper.getByName(params);
    }

    @Override
    public void modify(CompanyDO customer) {
        companyDaoMapper.modify(customer);
    }

    @Override
    public PagedData<CompanyDO> page(PagedRequest page, CompanySearchParams params) {
        Assert.notNull(page, "CompanyDaoImpl.page: page cannot be null!");
        Assert.notNull(page.getPageNumber(), "CompanyDaoImpl.page: page.pageNumber cannot be null!");
        Assert.notNull(page.getPageSize(), "CompanyDaoImpl.page: page.pageSize cannot be null!");
        var data = companyDaoMapper.page(page, params);
        var totalCount = companyDaoMapper.count(params);
        return new PagedData<>(data, totalCount);
    }

    @Override
    public void addCustomersToCompany(Long id, List<Long> ownerIds) {
        Assert.notNull(id, "id cannot be null!");
        Assert.notNull(ownerIds, "ownerIds cannot be null!");
        Assert.isTrue(ownerIds.size() > 0, "ownerIds cannot be empty!");
        var ids = companyDaoMapper.getOwnerIds(id);
        ids.forEach((o) -> {
            companyDaoMapper.removeOwner(id, o);
        });
//        var returnIds = new ArrayList<Long>();
//        ownerIds.forEach((o) ->{
//            if (!ids.contains(o)) {
//                returnIds.add(o);
//            }
//        });
        companyDaoMapper.connectCustomerCompany(id, ownerIds);
    }

    @Override
    public List<Long> getOwnerIds(Long id) {
        Assert.notNull(id, "id cannot be null!");
        return companyDaoMapper.getOwnerIds(id);
    }

    @Override
    public List<CustomerProfileDO> getOwners(List<Long> ownerIds) {
        Assert.notNull(ownerIds, "compIds cannot be null!");
        return companyDaoMapper.getOwners(ownerIds);
    }

    @Override
    public void removeOwner(Long companyId, Long customerId) {
        Assert.notNull(companyId, "companyId cannot be null!");
        Assert.notNull(customerId, "customerId cannot be null!");
        companyDaoMapper.removeOwner(companyId, customerId);
    }

    @Override
    public void delete(Long id) {
        Assert.notNull(id, "id cannot be null!");
        companyDaoMapper.delete(id);
        companyDaoMapper.deleteOwnerRelationship(id);
    }

    @Override
    public List<StoreIncomeDO> getStoreIncomes(List<Long> storeIds, Long days) {
        var actualDate = LocalDateTime.now();
        var response = new ArrayList<StoreIncomeDO>();
        for (int i = 1; i < days + 1; i++) {
            var storeIncomeDoElement = new StoreIncomeDO();
            var subDate = actualDate.minus(1, ChronoUnit.DAYS);
            var series =  companyDaoMapper.getStoreIncomes(storeIds,subDate,actualDate);
            series.forEach(a->a.setName(storeDaoMapper.getById((long) Integer.parseInt(a.getName())).getName()));
            storeIncomeDoElement.setName(subDate.toLocalDate().toString());
            storeIncomeDoElement.setSeries(series);
            response.add(storeIncomeDoElement);
            actualDate = subDate;
        }
        Collections.reverse(response);
        return response;
    }

    @Override
    public List<StoreIncomeDO> getStoreReports(Long storeId, Long days) {
        var actualDate = LocalDateTime.now();
        var response = new ArrayList<StoreIncomeDO>();
        var storeIncomeDoElement = new StoreIncomeDO();
        var storeIncomeDoElementExp = new StoreIncomeDO();
        storeIncomeDoElement.setName("Bevétel");
        storeIncomeDoElementExp.setName("Kiadás");

        var seriesDoList = new ArrayList<SeriesDO>();
        var seriesExpDoList = new ArrayList<SeriesDO>();

        for (int i = 1; i < days + 1; i++) {
            var subDate = actualDate.minus(1, ChronoUnit.DAYS);
            var value =  companyDaoMapper.getStoreReports(storeId,subDate,actualDate);
            var expenseValue =  companyDaoMapper.getExpenseValue(storeId,subDate,actualDate);
            var seriesDO = new SeriesDO();
            seriesDO.setName(subDate.toLocalDate().toString());
            seriesDO.setValue(value);
            if(value==null){
                seriesDO.setValue(0L);
            }
            seriesDoList.add(seriesDO);
            var seriesExpDO = new SeriesDO();
            seriesExpDO.setName(subDate.toLocalDate().toString());
            seriesExpDO.setValue(expenseValue);
            if(expenseValue==null){
                seriesExpDO.setValue(0L);

            }
            seriesExpDoList.add(seriesExpDO);
            actualDate = subDate;
        }
        storeIncomeDoElement.setSeries(seriesDoList);
        storeIncomeDoElementExp.setSeries(seriesExpDoList);
        response.add(storeIncomeDoElement);
        response.add(storeIncomeDoElementExp);
    return response;
    }
}
