package com.gergo.sb.dinnerboss.repository.admin;

import com.gergo.sb.dinnerboss.serviceapi.service.admin.Admin;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.AdminSessionDO;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.authentication.loginaction.CreateAdminSession;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.authentication.session.AdminSession;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.dao.AdminSessionDao;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

public class AdminSessionDaoImpl  implements AdminSessionDao {
    private AdminSessionDaoMapper adminSessionDaoMapper;

    public AdminSessionDaoImpl(AdminSessionDaoMapper mapper) {
        this.adminSessionDaoMapper = mapper;
    }

    @Override
    public Optional<AdminSessionDO> getAdminSession(String sessionId) {
        return adminSessionDaoMapper.findBySessionId(sessionId);
    }

    @Override
    public void generateSession(AdminSessionDO adminSessionDO) {
        adminSessionDaoMapper.removeExpiredSessions();
        String sessionId = adminSessionDO.getSessionId();
        if (sessionId == null) {
            sessionId = UUID.randomUUID().toString();
        }
        var target = new AdminSessionDO();
        target.setAdminEmail(adminSessionDO.getAdminEmail());
        target.setSessionId(adminSessionDO.getSessionId());
        target.setExpiry(expiry());
        adminSessionDaoMapper.insert(target);
    }

    private LocalDateTime expiry() {
        long EXPIRY_SECONDS = 600L;
        return LocalDateTime.now().plus(EXPIRY_SECONDS, ChronoUnit.SECONDS);
    }
//
//    @Override
//    public  generateSession(Admin admin) {
//
//        adminSessionDaoMapper.removeExpiredSessions();
//        String sessionId = UUID.randomUUID().toString();
//        AdminSessionDO adminSession = new AdminSessionDO(admin.getEmail(), sessionId, expiry());
//        adminSessionDaoMapper.insert(adminSession);
//        return AdminSession.getInstance(adminSession);
//    }
//    private LocalDateTime expiry() {
//        long EXPIRY_SECONDS = 600L;
//        return LocalDateTime.now().plus(EXPIRY_SECONDS, ChronoUnit.SECONDS);
//    }
}
