package com.gergo.sb.dinnerboss.repository.admin;


import com.gergo.sb.dinnerboss.serviceapi.service.admin.Admin;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.CreateAdminDO;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.dao.AdminDao;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.Map;

public class AdminDaoImpl implements AdminDao {
    private AdminDaoMapper adminDaoMapper;

    public AdminDaoImpl(AdminDaoMapper adminDaoMapper) {
        this.adminDaoMapper = adminDaoMapper;
    }

    @Override
    public void create(CreateAdminDO createAdminDO) {
        adminDaoMapper.create(createAdminDO);
    }

    @Override
    public Admin get(String email) {
        Assert.notNull(email, "Email cannot be null!");
        Map<String, Object> params = Collections.singletonMap("email", email);
        return adminDaoMapper.get(params);
    }
}
