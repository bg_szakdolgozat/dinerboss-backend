package com.gergo.sb.dinnerboss.repository.company;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.company.*;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerSearchParams;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface CompanyDaoMapper {
    void create(CreateCompanyDO company);

    CompanyDO getByName(Map<String, Object> params);

    void modify(@Param("company") CompanyDO customer);

    List<CompanyDO> page(@Param("page") PagedRequest page,@Param("param") CompanySearchParams params);

    Long count(@Param("param") CompanySearchParams params);

    void connectCustomerCompany(@Param("id") Long id,@Param("ownerIds") List<Long> ownerIds);

    List<Long> getOwnerIds(@Param("id") Long id);

    List<CustomerProfileDO> getOwners(@Param("ownerIds") List<Long> ownerIds);

    void removeOwner(@Param("companyId")Long companyId, @Param("customerId")Long customerId);

    void delete(@Param("id")Long id);

    void deleteOwnerRelationship(@Param("id") Long id);

    List<SeriesDO> getStoreIncomes(@Param("storeIds") List<Long> storeIds, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to);

    Long getStoreReports( @Param("storeId")Long storeId,@Param("from")LocalDateTime subDate, @Param("to") LocalDateTime actualDate);

    Long getExpenseValue(@Param("storeId") Long storeId,@Param("from") LocalDateTime subDate, @Param("to") LocalDateTime actualDate);
}
