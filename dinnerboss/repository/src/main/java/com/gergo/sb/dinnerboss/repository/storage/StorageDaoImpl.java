package com.gergo.sb.dinnerboss.repository.storage;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.storage.*;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.storage.StorageDao;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

public class StorageDaoImpl implements StorageDao {


    private StorageDaoMapper storageDaoMapper;

    public StorageDaoImpl(StorageDaoMapper storageDaoMapper) {
        this.storageDaoMapper = storageDaoMapper;
    }

    @Override
    public void create(Long storeId) {
        storageDaoMapper.create(storeId);
    }

    @Override
    public void delete(Long id) {
        storageDaoMapper.delete(id);

    }

    @Override
    public void registerCommodity(RegisterDO request) {
        storageDaoMapper.registerCommodity(request);
    }

    @Override
    public PagedData<RemoteCommodityRegDO> page(PagedRequest page, PageRemoteCommRegistrationsSearch params) {
        Assert.notNull(page, "StorageDaoImpl.page: page cannot be null!");
        Assert.notNull(page.getPageNumber(), "StorageDaoImpl.page: page.pageNumber cannot be null!");
        Assert.notNull(page.getPageSize(), "StorageDaoImpl.page: page.pageSize cannot be null!");
        var data = storageDaoMapper.page(page, params);
        data.forEach((a) -> {
            a.setCommodity(storageDaoMapper.getCommodityById(a.getComId()));
            a.setCustomer(storageDaoMapper.getCustomerById(a.getCustomerId()));
        });
        var totalCount = storageDaoMapper.count(params);
        return new PagedData<>(data, totalCount);
    }

    @Override
    public void registerTool(RegisterDO request) {
        storageDaoMapper.registerTool(request);
    }

    @Override
    public PagedData<RemoteCommodityRegDO> pageToolReg(PagedRequest page, PageRemoteCommRegistrationsSearch params) {
        Assert.notNull(page, "StorageDaoImpl.pageToolReg: page cannot be null!");
        Assert.notNull(page.getPageNumber(), "StorageDaoImpl.pageToolReg: page.pageNumber cannot be null!");
        Assert.notNull(page.getPageSize(), "StorageDaoImpl.pageToolReg: page.pageSize cannot be null!");
        var data = storageDaoMapper.pageToolReg(page, params);
        data.forEach((a) -> {
            a.setCommodity(storageDaoMapper.getToolById(a.getComId()));
            a.setCustomer(storageDaoMapper.getCustomerById(a.getCustomerId()));
        });
        var totalCount = storageDaoMapper.count(params);
        return new PagedData<>(data, totalCount);
    }

    @Override
    public List<StorageContentDO> listCommodityByStorage(Long storageId) {
        Assert.notNull(storageId, "storageId cannot be null!");
        var commodityIds = storageDaoMapper.getCommIdsByStorageId(storageId);
        List<StorageContentDO> data = new ArrayList<StorageContentDO>();
        commodityIds.forEach((a) -> {
            var inValues = storageDaoMapper.getInValues(a,storageId);
            var outValues = storageDaoMapper.getOutValues(a,storageId);
            var totalCount = inValues;
            if (outValues != null) {
                totalCount = inValues - outValues;
                if (totalCount < 0) {
                    totalCount = 0L;
                }
            }
            var commodity = storageDaoMapper.getCommodityById(a);
            var storageContent = new StorageContentDO();
            storageContent.setTotal(totalCount);
            storageContent.setCommodity(commodity);
            if(commodity!=null){
                data.add(storageContent);
            }
        });
        return data;
    }

    @Override
    public List<StorageContentDO> listToolByStorage(Long storageId) {
        Assert.notNull(storageId, "storageId cannot be null!");
        var commodityIds = storageDaoMapper.getToolIdsByStorageId(storageId);
        List<StorageContentDO> data = new ArrayList<StorageContentDO>();
        commodityIds.forEach((a) -> {
            var inValues = storageDaoMapper.getToolInValues(a,storageId);
            var outValues = storageDaoMapper.getToolOutValues(a,storageId);
            var totalCount = inValues;
            if (outValues != null) {
                totalCount = inValues - outValues;
                if (totalCount < 0) {
                    totalCount = 0L;
                }
            }
            var commodity = storageDaoMapper.getToolById(a);
            var storageContent = new StorageContentDO();
            storageContent.setTotal(totalCount);
            storageContent.setCommodity(commodity);
            data.add(storageContent);
        });
        return data;
    }

    @Override
    public List<StorageDO> getAllByStore(List<Long> storeIds) {
        Assert.notNull(storeIds, "storeId cannot be null!");
        return storageDaoMapper.getAllByStore(storeIds);
    }

    @Override
    public List<StorageContentDO> getConsumableCommodities(Long storageId,Long limit) {
        var list = this.listCommodityByStorage(storageId);
        var returnList = new ArrayList<StorageContentDO>();
        list.forEach((a)->{
            if(a.getTotal()<limit){
                returnList.add(a);
            }
        });
        return returnList;
    }

    @Override
    public StorageDO getById(Long id) {
        return storageDaoMapper.getById(id);
    }
}
