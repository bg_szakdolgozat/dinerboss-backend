package com.gergo.sb.dinnerboss.repository.report;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.report.CreateReportDO;
import com.gergo.sb.dinnerboss.serviceapi.service.report.ReportDO;
import com.gergo.sb.dinnerboss.serviceapi.service.report.ReportSearchParams;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.report.ReportDao;
import org.springframework.util.Assert;

public class ReportDaoImpl implements ReportDao {

    private ReportDaoMapper reportDaoMapper;

    public ReportDaoImpl(ReportDaoMapper reportDaoMapper) {
        this.reportDaoMapper = reportDaoMapper;
    }

    @Override
    public void create(CreateReportDO report) {
        reportDaoMapper.create(report);
    }

    @Override
    public PagedData<ReportDO> page(PagedRequest page, ReportSearchParams params) {
        Assert.notNull(page, "ReportDaoImpl.page: page cannot be null!");
        Assert.notNull(page.getPageNumber(), "ReportDaoImpl.page: page.pageNumber cannot be null!");
        Assert.notNull(page.getPageSize(), "ReportDaoImpl.page: page.pageSize cannot be null!");
        var data = reportDaoMapper.page( page,params);
        var totalCount = reportDaoMapper.count(params);
        return new PagedData<>(data, totalCount);    }
}
