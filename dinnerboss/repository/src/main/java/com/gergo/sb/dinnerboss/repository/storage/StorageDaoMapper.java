package com.gergo.sb.dinnerboss.repository.storage;

import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommodityDO;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.storage.PageRemoteCommRegistrationsSearch;
import com.gergo.sb.dinnerboss.serviceapi.service.storage.RegisterDO;
import com.gergo.sb.dinnerboss.serviceapi.service.storage.RemoteCommodityRegDO;
import com.gergo.sb.dinnerboss.serviceapi.service.storage.StorageDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StorageDaoMapper {
    void create(@Param("storeId") Long storeId);

    void delete(Long id);

    void registerCommodity(RegisterDO request);

    List<RemoteCommodityRegDO> page(@Param("page") PagedRequest page, @Param("param") PageRemoteCommRegistrationsSearch params);

    CommodityDO getCommodityById(@Param("comId") Long comId);

    CustomerProfileDO getCustomerById(@Param("customerId") Long customerId);

    Long count(@Param("param") PageRemoteCommRegistrationsSearch params);

    void registerTool(RegisterDO request);

    List<RemoteCommodityRegDO> pageToolReg(@Param("page") PagedRequest page, @Param("param") PageRemoteCommRegistrationsSearch params);

    CommodityDO getToolById(@Param("comId") Long comId);

    List<Long> getCommIdsByStorageId(@Param("storageId") Long storageId);

    Long getInValues(@Param("comId") Long comId,@Param("storageId") Long storageId);

    Long getOutValues(@Param("comId") Long a,@Param("storageId") Long storageId);

    List<Long> getToolIdsByStorageId(@Param("storageId") Long storageId);

    Long getToolInValues(@Param("comId") Long comId,@Param("storageId") Long storageId);

    Long getToolOutValues(@Param("comId") Long comId,@Param("storageId") Long storageId);

    List<StorageDO> getAllByStore(@Param("storeIds") List<Long> storeIds);

    StorageDO getById(@Param("id") Long id);
}
