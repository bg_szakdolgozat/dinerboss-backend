package com.gergo.sb.dinnerboss.repository.commodity;

import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommodityDO;
import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommoditySearchParams;
import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CreateCommodityDO;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.commodity.CommodityDao;
import org.springframework.util.Assert;

public class CommodityDaoImpl implements CommodityDao {

    private CommodityDaoMapper commodityDaoMapper;

    public CommodityDaoImpl(CommodityDaoMapper commodityDaoMapper) {
        this.commodityDaoMapper = commodityDaoMapper;
    }

    @Override
    public void create(CreateCommodityDO commodity) {
        this.commodityDaoMapper.create(commodity);
    }

    @Override
    public PagedData<CommodityDO> page(PagedRequest page, CommoditySearchParams params) {
        Assert.notNull(page, "CommodityDaoImpl.page: page cannot be null!");
        Assert.notNull(page.getPageNumber(), "CommodityDaoImpl.page: page.pageNumber cannot be null!");
        Assert.notNull(page.getPageSize(), "CommodityDaoImpl.page: page.pageSize cannot be null!");
        var data = commodityDaoMapper.page(page, params);
        var totalCount = commodityDaoMapper.count(params);
        return new PagedData<>(data, totalCount);
    }

    @Override
    public void modify(CommodityDO remoteCommodity) {
        Assert.notNull(remoteCommodity, "remoteCommodity cannot be null!");
        commodityDaoMapper.modify(remoteCommodity);
    }

    @Override
    public void delete(Long id) {
        Assert.notNull(id, "id cannot be null!");
        commodityDaoMapper.delete(id);
    }

    @Override
    public void createTool(CreateCommodityDO tool) {
        this.commodityDaoMapper.createTool(tool);

    }

    @Override
    public PagedData<CommodityDO> pageTools(PagedRequest page, CommoditySearchParams params) {
        Assert.notNull(page, "CommodityDaoImpl.page TOOLS: page cannot be null!");
        Assert.notNull(page.getPageNumber(), "CommodityDaoImpl.page TOOLS: page.pageNumber cannot be null!");
        Assert.notNull(page.getPageSize(), "CommodityDaoImpl.page TOOLS: page.pageSize cannot be null!");
        var data = commodityDaoMapper.pageTools(page, params);
        var totalCount = commodityDaoMapper.countTools(params);
        return new PagedData<>(data, totalCount);
    }

    @Override
    public void deleteTool(Long id) {
        Assert.notNull(id, "id cannot be null!");
        commodityDaoMapper.deleteTool(id);
    }


}
