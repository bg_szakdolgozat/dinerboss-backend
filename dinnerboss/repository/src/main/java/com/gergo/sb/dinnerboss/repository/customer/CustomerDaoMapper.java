package com.gergo.sb.dinnerboss.repository.customer;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.company.CompanyDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CreateCustomerDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerSearchParams;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CustomerDaoMapper {

    void create(CreateCustomerDO createCustomerDO);

    CustomerProfileDO getByEmail(Map<String, Object> params);

    List<CustomerProfileDO> page(@Param("page") PagedRequest page, @Param("param") CustomerSearchParams params);

    Long count(@Param("param") CustomerSearchParams params);

    void modify(@Param("customer") CustomerProfileDO customer);

    void connectCustomerCompany(@Param("id") Long id, @Param("compIds") List<Long> compIds);

    List<CompanyDO> getCompanies(@Param("compIds") List<Long> compIds);

    List<Long> getCompIds(@Param("id") Long id);

    void delete(@Param("id") Long id);

    void deleteOwnerRelationship(@Param("id") Long id);

    CustomerProfileDO loginAdmin(@Param("email")String email, @Param("password")String password);

    List<Long> getStoreIds(@Param("id") Long id);

    List<StoreDO> getStores(@Param("storeIds") List<Long> storeIds);


    List<Long> getCustomerIdsByStoreIds(@Param("storeIds") List<Long> storeIds);

    CustomerProfileDO getById(Map<String, Object> params);

    void setCheckoutMoney(@Param("id") Long id,@Param("money") Long money);

    void extractCheckoutMoney(@Param("id") Long id,@Param("money") Long money);
}
