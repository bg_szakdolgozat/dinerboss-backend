package com.gergo.sb.dinnerboss.repository.checkout;

import com.gergo.sb.dinnerboss.serviceapi.service.checkout.*;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CheckoutDaoMapper {

    void create(CreateCheckoutDO checkoutDO);

    List<CheckoutDO> page(@Param("page") PagedRequest page,@Param("param")  CheckoutSearchParams params);

    Long count(@Param("param") CheckoutSearchParams params);

    Long getStoreId(@Param("id") Long id);

    void delete(@Param("id") Long id);

    void modify(@Param("checkout") ModifyCheckoutDO remoteCheckout);

    void addTransmission(AddTransmissionDO checkout);

    List<TransmissionDO> pageTransmissions(@Param("page") PagedRequest page, @Param("param") TransmissionSearchParams params);

    Long countTransmissions(@Param("param") TransmissionSearchParams params);
}
