package com.gergo.sb.dinnerboss.repository.customer;

import com.gergo.sb.dinnerboss.repository.store.StoreDaoMapper;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.company.CompanyDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CreateCustomerDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerSearchParams;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreDO;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.customer.dao.CustomerDao;
import org.springframework.util.Assert;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class CustomerDaoImpl implements CustomerDao {

    private CustomerDaoMapper customerDaoMapper;
    private StoreDaoMapper storeDaoMapper;

    public CustomerDaoImpl(CustomerDaoMapper customerDaoMapper, StoreDaoMapper storeDaoMapper) {
        this.customerDaoMapper = customerDaoMapper;
        this.storeDaoMapper = storeDaoMapper;
    }

    @Override
    public void create(CreateCustomerDO customer) {
        customerDaoMapper.create(customer);
    }

    @Override
    public CustomerProfileDO getByEmail(String email) {
        Assert.notNull(email, "Email cannot be null!");
        Map<String, Object> params = Collections.singletonMap("email", email);
        return customerDaoMapper.getByEmail(params);
    }

    @Override
    public PagedData<CustomerProfileDO> page(PagedRequest page, CustomerSearchParams params) {
        Assert.notNull(page, "CustomerDaoImpl.page: page cannot be null!");
        Assert.notNull(page.getPageNumber(), "CustomerDaoImpl.page: page.pageNumber cannot be null!");
        Assert.notNull(page.getPageSize(), "CustomerDaoImpl.page: page.pageSize cannot be null!");
        if(params.getStoreIds()!=null && !params.getStoreIds().isEmpty()){
            var customerIds = customerDaoMapper.getCustomerIdsByStoreIds(params.getStoreIds());
            var selectedCustIds = new ArrayList<Long>();
            customerIds.forEach((a -> {
                if(!selectedCustIds.contains(a)){
                    selectedCustIds.add(a);
                }
            }));
            params.setCustomerIds(selectedCustIds);
        }
        var data = customerDaoMapper.page(page, params);
        var totalCount = customerDaoMapper.count(params);
        return new PagedData<>(data, totalCount);
    }

    @Override
    public void modify(CustomerProfileDO customer) {
        customerDaoMapper.modify(customer);
    }

    @Override
    public void addCompaniesToCustomer(Long id, List<Long> compIds) {
        Assert.notNull(id, "id cannot be null!");
        Assert.notNull(compIds, "compIds cannot be null!");
        Assert.isTrue(compIds.size() > 0, "compIds cannot be empty!");
        customerDaoMapper.connectCustomerCompany(id, compIds);
    }

    @Override
    public List<CompanyDO> getCompanies(List<Long> compIds) {
        Assert.notNull(compIds, "compIds cannot be null!");
        return customerDaoMapper.getCompanies(compIds);
    }

    @Override
    public List<StoreDO> getStores(List<Long> storeIds) {
        Assert.notNull(storeIds, "storeIds cannot be null!");
        return customerDaoMapper.getStores(storeIds);
    }

    @Override
    public void delete(Long id) {
        Assert.notNull(id, "id cannot be null!");
        customerDaoMapper.delete(id);
        customerDaoMapper.deleteOwnerRelationship(id);
    }

    @Override
    public CustomerProfileDO loginAdmin(String email, String password) {
        Assert.notNull(email, "Email cannot be null!");
        Assert.notNull(password, "password cannot be null!");
        return customerDaoMapper.loginAdmin(email,password);
    }

    @Override
    public CustomerProfileDO getById(Long id) {
        Assert.notNull(id, "Id cannot be null!");
        Map<String, Object> params = Collections.singletonMap("id", id);
        return customerDaoMapper.getById(params);
    }

    @Override
    public void setCheckoutMoney(Long id, Boolean in, Long money) {
        Assert.notNull(id, "Id cannot be null!");
        Assert.notNull(in, "in cannot be null!");
        Assert.notNull(money, "money cannot be null!");
        if(in){
            customerDaoMapper.setCheckoutMoney(id,money);
        }else{
            customerDaoMapper.extractCheckoutMoney(id,money);
        }
    }

    @Override
    public List<Long> getCompIds(Long id) {
        Assert.notNull(id, "id cannot be null!");
        return customerDaoMapper.getCompIds(id);
    }

    @Override
    public List<Long> getStoreIds(Long id) {
        Assert.notNull(id, "id cannot be null!");
        return customerDaoMapper.getStoreIds(id);
    }
}
