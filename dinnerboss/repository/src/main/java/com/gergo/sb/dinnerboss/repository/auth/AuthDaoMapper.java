package com.gergo.sb.dinnerboss.repository.auth;

import com.gergo.sb.dinnerboss.serviceapi.service.auth.RemoteAdminProfile;
import org.apache.ibatis.annotations.Param;

public interface AuthDaoMapper {

    RemoteAdminProfile checkPassword(@Param("adminEmail") String adminEmail,@Param("password") String password);

}
