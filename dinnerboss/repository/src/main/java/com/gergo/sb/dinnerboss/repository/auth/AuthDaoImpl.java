package com.gergo.sb.dinnerboss.repository.auth;

import com.gergo.sb.dinnerboss.repository.admin.AdminDaoMapper;
import com.gergo.sb.dinnerboss.serviceapi.service.auth.RemoteAdminProfile;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.auth.dao.AuthDao;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.Map;

public class AuthDaoImpl implements AuthDao {
    private AuthDaoMapper authDaoMapper;

    public AuthDaoImpl(AuthDaoMapper authDaoMapper) {
        this.authDaoMapper = authDaoMapper;
    }

    @Override
    public RemoteAdminProfile checkPassword(String adminEmail, String password) {
        Assert.notNull(adminEmail, "Email cannot be null!");
        Assert.notNull(password, "Password cannot be null!");
        return authDaoMapper.checkPassword(adminEmail,password);
    }
}
