package com.gergo.sb.dinnerboss.repository.store;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.company.CompanyDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.store.CreateStoreDO;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreDO;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreSearchParams;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StoreDaoMapper {

    void create(CreateStoreDO company);

    StoreDO getById(@Param("id") Long id);

    Long getCompanyIdByStore(@Param("id") Long id);

    CompanyDO getCompanyById(@Param("companyId") Long companyId);

    void delete(Long id);

    void modify(@Param("store") StoreDO remoteStore);

    List<StoreDO> page(@Param("page") PagedRequest page,@Param("param") StoreSearchParams params);

    Long count(@Param("param")StoreSearchParams params);

    List<StoreDO> getStoresByCompId(@Param("id")Long id);

    StoreDO getByName(@Param("name")String name);

    void assignUsersToStore(@Param("id")Long id,@Param("userIds") List<Long> userIds);

    void revokeUsersFromStore(@Param("id")Long storeId, @Param("userIds")List<Long> userIds);

    List<Long> getUserIds(@Param("id")Long id);

    List<CustomerProfileDO> getCustomers(@Param("customerIds")List<Long> customerIds);
}
