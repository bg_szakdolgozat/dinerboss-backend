package com.gergo.sb.dinnerboss.repository.checkout;

import com.gergo.sb.dinnerboss.serviceapi.service.checkout.*;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.checkout.CheckoutDao;
import org.apache.ibatis.annotations.Param;
import org.springframework.util.Assert;

public class CheckoutDaoImpl implements CheckoutDao {

    private CheckoutDaoMapper checkoutDaoMapper;

    public CheckoutDaoImpl(CheckoutDaoMapper checkoutDaoMapper) {
        this.checkoutDaoMapper = checkoutDaoMapper;
    }

    @Override
    public void create(CreateCheckoutDO createCheckoutDO) {
        this.checkoutDaoMapper.create(createCheckoutDO);

    }

    @Override
    public PagedData<CheckoutDO> page(PagedRequest page, CheckoutSearchParams params) {
        Assert.notNull(page, "CheckoutDaoImpl.page: page cannot be null!");
        Assert.notNull(page.getPageNumber(), "CheckoutDaoImpl.page: page.pageNumber cannot be null!");
        Assert.notNull(page.getPageSize(), "CheckoutDaoImpl.page: page.pageSize cannot be null!");
        var data = checkoutDaoMapper.page( page,params);
        var totalCount = checkoutDaoMapper.count(params);
        return new PagedData<>(data, totalCount);
    }

    @Override
    public Long getStoreId(Long id) {
        Assert.notNull(id, "id cannot be null!");

        return checkoutDaoMapper.getStoreId(id);
    }

    @Override
    public void delete(Long id) {
        Assert.notNull(id, "id cannot be null!");
        checkoutDaoMapper.delete(id);
    }

    @Override
    public void modify(ModifyCheckoutDO remoteCheckout) {
        this.checkoutDaoMapper.modify(remoteCheckout);
    }

    @Override
    public void addTransmission(AddTransmissionDO checkout) {
        this.checkoutDaoMapper.addTransmission(checkout);

    }

    @Override
    public PagedData<TransmissionDO> pageTransmissions(PagedRequest page, TransmissionSearchParams params) {
        Assert.notNull(page, "CheckoutDaoImpl.pageTransmissions: page cannot be null!");
        Assert.notNull(page.getPageNumber(), "CheckoutDaoImpl.pageTransmissions: page.pageNumber cannot be null!");
        Assert.notNull(page.getPageSize(), "CheckoutDaoImpl.pageTransmissions: page.pageSize cannot be null!");
        var data = checkoutDaoMapper.pageTransmissions( page,params);
        var totalCount = checkoutDaoMapper.countTransmissions(params);
        return new PagedData<>(data, totalCount);
    }
}
