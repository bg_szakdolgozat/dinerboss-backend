CREATE TABLE `CUSTOMER_STORE`
(
    `ID`         BIGINT(20)     NOT NULL AUTO_INCREMENT,
    `SINCE`      datetime   NOT NULL,
    `ROLE` VARCHAR(100) not null ,
    `CUSTOMER_ID` BIGINT(20)   NOT NULL,
    `STORE_ID`  BIGINT(20)   NOT NULL,
    PRIMARY KEY (`ID`)
) COLLATE = 'utf8mb4_unicode_ci'
  ENGINE = InnoDB;