CREATE TABLE `REPORT`
(
    `ID`         BIGINT(20)     NOT NULL AUTO_INCREMENT,
    `DATE` datetime   NOT NULL,
    `COMMENT`      VARCHAR(120),
    `INCOME`      BIGINT(20) ,
    `EXPENSE` BIGINT(20) ,
    `CUSTOMER_ID`      BIGINT(20) NOT NULL,
    `STORE_ID`      BIGINT(20) NOT NULL,
    PRIMARY KEY (`ID`)
) COLLATE = 'utf8mb4_unicode_ci'
  ENGINE = InnoDB;