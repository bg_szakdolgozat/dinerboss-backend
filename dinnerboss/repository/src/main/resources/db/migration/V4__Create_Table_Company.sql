CREATE TABLE `COMPANY`
(
    `ID`         BIGINT(20)     NOT NULL AUTO_INCREMENT,
    `NAME`      VARCHAR(50)    NOT NULL,
    `FOUNDED` datetime   NOT NULL,
    `ADDRESS`  VARCHAR(100)   NOT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE (`NAME`)
) COLLATE = 'utf8mb4_unicode_ci'
  ENGINE = InnoDB;
