CREATE TABLE `CHECKOUT`
(
    `ID`         BIGINT(20)     NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(120),
    `MONEY` BIGINT(20)   NOT NULL DEFAULT 0,
    `COMMENT`      VARCHAR(120),
    `STORE_ID`      BIGINT(20) NOT NULL,
    PRIMARY KEY (`ID`)
) COLLATE = 'utf8mb4_unicode_ci'
  ENGINE = InnoDB;

CREATE TABLE `TRANSMISSION`
(
    `ID`         BIGINT(20)     NOT NULL AUTO_INCREMENT,
    `DATE` datetime   NOT NULL,
    `COMMENT`      VARCHAR(120),
    `IN`      BOOLEAN NOT NULL,
    `CUSTOMER_ID` BIGINT(20)   NOT NULL,
    `CHECKOUT_ID`      BIGINT(20) NOT NULL,
    PRIMARY KEY (`ID`)
) COLLATE = 'utf8mb4_unicode_ci'
  ENGINE = InnoDB;