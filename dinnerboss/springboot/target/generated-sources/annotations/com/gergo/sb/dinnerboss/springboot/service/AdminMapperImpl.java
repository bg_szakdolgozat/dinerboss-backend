package com.gergo.sb.dinnerboss.springboot.service;

import com.gergo.sb.dinnerboss.serviceapi.service.admin.Admin;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.CreateAdminDO;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.CreateRemoteAdminResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.GetRemoteAdminResponse;
import com.gergo.sb.dinnerboss.springboot.model.AdminDTO;
import com.gergo.sb.dinnerboss.springboot.model.CreateAdminDTO;
import com.gergo.sb.dinnerboss.springboot.model.CreateAdminResponse;
import com.gergo.sb.dinnerboss.springboot.model.GetAdminResponse;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-03-21T14:13:45+0100",
    comments = "version: 1.3.0.Final, compiler: javac, environment: Java 11.0.7 (Oracle Corporation)"
)
public class AdminMapperImpl implements AdminMapper {

    @Override
    public CreateAdminDO map(CreateAdminDTO createAdminDTO) {
        if ( createAdminDTO == null ) {
            return null;
        }

        CreateAdminDO createAdminDO = new CreateAdminDO();

        createAdminDO.setId( createAdminDTO.getId() );
        createAdminDO.setPassword( createAdminDTO.getPassword() );
        createAdminDO.setFirstName( createAdminDTO.getFirstName() );
        createAdminDO.setLastName( createAdminDTO.getLastName() );
        createAdminDO.setEmail( createAdminDTO.getEmail() );
        createAdminDO.setPincode( createAdminDTO.getPincode() );

        return createAdminDO;
    }

    @Override
    public CreateAdminResponse map(CreateRemoteAdminResponse createRemoteAdminResponse) {
        if ( createRemoteAdminResponse == null ) {
            return null;
        }

        CreateAdminResponse createAdminResponse = new CreateAdminResponse();

        createAdminResponse.setRc( createRemoteAdminResponse.getRc() );

        return createAdminResponse;
    }

    @Override
    public GetAdminResponse map(GetRemoteAdminResponse getRemoteAdminResponse) {
        if ( getRemoteAdminResponse == null ) {
            return null;
        }

        GetAdminResponse getAdminResponse = new GetAdminResponse();

        getAdminResponse.setRc( getRemoteAdminResponse.getRc() );
        getAdminResponse.setAdmin( adminToAdminDTO( getRemoteAdminResponse.getAdmin() ) );

        return getAdminResponse;
    }

    protected AdminDTO adminToAdminDTO(Admin admin) {
        if ( admin == null ) {
            return null;
        }

        AdminDTO adminDTO = new AdminDTO();

        adminDTO.setId( admin.getId() );
        adminDTO.setFirstName( admin.getFirstName() );
        adminDTO.setLastName( admin.getLastName() );
        adminDTO.setEmail( admin.getEmail() );

        return adminDTO;
    }
}
