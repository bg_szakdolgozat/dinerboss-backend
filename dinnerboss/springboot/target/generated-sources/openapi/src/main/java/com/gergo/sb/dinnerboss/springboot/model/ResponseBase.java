package com.gergo.sb.dinnerboss.springboot.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import java.io.Serializable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ResponseBase
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-03-21T14:13:29.605433600+01:00[Europe/Prague]")

public class ResponseBase  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("rc")
  private Integer rc;

  public ResponseBase rc(Integer rc) {
    this.rc = rc;
    return this;
  }

  /**
   * Get rc
   * minimum: 0
   * maximum: 999
   * @return rc
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Min(0) @Max(999) 
  public Integer getRc() {
    return rc;
  }

  public void setRc(Integer rc) {
    this.rc = rc;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResponseBase responseBase = (ResponseBase) o;
    return Objects.equals(this.rc, responseBase.rc);
  }

  @Override
  public int hashCode() {
    return Objects.hash(rc);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ResponseBase {\n");
    
    sb.append("    rc: ").append(toIndentedString(rc)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

