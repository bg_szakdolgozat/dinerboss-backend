package com.gergo.sb.dinnerboss.springboot.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.gergo.sb.dinnerboss.springboot.model.CreateAdminDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import java.io.Serializable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CreateAdminRequest
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-03-21T14:13:29.605433600+01:00[Europe/Prague]")

public class CreateAdminRequest  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("admin")
  private CreateAdminDTO admin;

  public CreateAdminRequest admin(CreateAdminDTO admin) {
    this.admin = admin;
    return this;
  }

  /**
   * Get admin
   * @return admin
  */
  @ApiModelProperty(value = "")

  @Valid

  public CreateAdminDTO getAdmin() {
    return admin;
  }

  public void setAdmin(CreateAdminDTO admin) {
    this.admin = admin;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateAdminRequest createAdminRequest = (CreateAdminRequest) o;
    return Objects.equals(this.admin, createAdminRequest.admin);
  }

  @Override
  public int hashCode() {
    return Objects.hash(admin);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateAdminRequest {\n");
    
    sb.append("    admin: ").append(toIndentedString(admin)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

