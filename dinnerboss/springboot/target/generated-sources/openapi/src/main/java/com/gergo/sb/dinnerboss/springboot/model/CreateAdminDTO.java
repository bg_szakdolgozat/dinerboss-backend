package com.gergo.sb.dinnerboss.springboot.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import java.io.Serializable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CreateAdminDTO
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-03-21T14:13:29.605433600+01:00[Europe/Prague]")

public class CreateAdminDTO  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("id")
  private String id;

  @JsonProperty("password")
  private String password;

  @JsonProperty("firstName")
  private String firstName;

  @JsonProperty("lastName")
  private String lastName;

  @JsonProperty("email")
  private String email;

  @JsonProperty("pincode")
  private Long pincode;

  public CreateAdminDTO id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  @ApiModelProperty(example = "ntomic", required = true, value = "")
  @NotNull

@Pattern(regexp="[A-Za-z0-9._-]*") @Size(max=16) 
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public CreateAdminDTO password(String password) {
    this.password = password;
    return this;
  }

  /**
   * Get password
   * @return password
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Size(min=8,max=64) 
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public CreateAdminDTO firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  /**
   * Get firstName
   * @return firstName
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="[A-Za-z0-9ÁÉÍÓÖŐÚÜŰáéíóöőúüű\\p{L}][A-Za-z0-9ÁÉÍÓÖŐÚÜŰáéíóöőúüűp{L}.\\- ]*") @Size(min=1,max=30) 
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public CreateAdminDTO lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  /**
   * Get lastName
   * @return lastName
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="[A-Za-z0-9ÁÉÍÓÖŐÚÜŰáéíóöőúüű\\p{L}][A-Za-z0-9ÁÉÍÓÖŐÚÜŰáéíóöőúüűp{L}.\\- ]*") @Size(min=1,max=30) 
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public CreateAdminDTO email(String email) {
    this.email = email;
    return this;
  }

  /**
   * Get email
   * @return email
  */
  @ApiModelProperty(example = "email@example.com", required = true, value = "")
  @NotNull

@Pattern(regexp="[A-Za-z0-9_=]+([-+.'][A-Za-z0-9_=]+)*@[A-Za-z0-9_]+([-.][A-Za-z0-9_]+)*.[A-Za-z0-9_]+([-.][A-Za-z0-9_]+)*") @Size(min=1,max=255) 
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public CreateAdminDTO pincode(Long pincode) {
    this.pincode = pincode;
    return this;
  }

  /**
   * Get pincode
   * @return pincode
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Long getPincode() {
    return pincode;
  }

  public void setPincode(Long pincode) {
    this.pincode = pincode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateAdminDTO createAdminDTO = (CreateAdminDTO) o;
    return Objects.equals(this.id, createAdminDTO.id) &&
        Objects.equals(this.password, createAdminDTO.password) &&
        Objects.equals(this.firstName, createAdminDTO.firstName) &&
        Objects.equals(this.lastName, createAdminDTO.lastName) &&
        Objects.equals(this.email, createAdminDTO.email) &&
        Objects.equals(this.pincode, createAdminDTO.pincode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, password, firstName, lastName, email, pincode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateAdminDTO {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    pincode: ").append(toIndentedString(pincode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

