package com.gergo.sb.dinnerboss.springboot.security.credentials;

import org.springframework.util.Assert;

public class Email {
    private final String value;

    public Email(String value) {
        Assert.notNull(value, "Email cannot be null");
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
