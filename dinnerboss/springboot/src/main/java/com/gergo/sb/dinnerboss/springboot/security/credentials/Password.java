package com.gergo.sb.dinnerboss.springboot.security.credentials;

import org.springframework.util.Assert;

public class Password {

    private final String value;

    public Password(String value) {
        Assert.notNull(value, "Password cannot be null");
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
