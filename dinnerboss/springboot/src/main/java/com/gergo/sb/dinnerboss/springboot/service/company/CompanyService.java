package com.gergo.sb.dinnerboss.springboot.service.company;

import com.gergo.sb.dinnerboss.serviceapi.service.company.CompanyDO;
import com.gergo.sb.dinnerboss.serviceapi.service.company.CompanyServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.company.CreateRemoteCompanyRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.company.ModifyRemoteCompanyRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.ModifyRemoteCustomerRequest;
import com.gergo.sb.dinnerboss.springboot.api.CompanyApiDelegate;
import com.gergo.sb.dinnerboss.springboot.model.*;
import com.gergo.sb.dinnerboss.springboot.service.common.PagingMapper;
import com.gergo.sb.dinnerboss.springboot.service.customer.CustomerMapper;
import org.springframework.http.ResponseEntity;

public class CompanyService implements CompanyApiDelegate {

    private CompanyServiceApi companyServiceApi;

    public CompanyService(CompanyServiceApi companyServiceApi) {
        this.companyServiceApi = companyServiceApi;
    }

    @Override
    public ResponseEntity<CreateCompanyResponse> createCompany(CreateCompanyRequest createCompanyRequest) {
        var admin = new CreateRemoteCompanyRequest(CompanyMapper.INSTANCE.map(createCompanyRequest.getCompany()));
        var response = companyServiceApi.createCompany(admin);
        return ResponseEntity.ok(CompanyMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<GetCompanyByNameResponse> getCompanyByName(GetCompanyByNameRequest request) {
        var response = companyServiceApi.getCompanyByName(request.getName());
        return ResponseEntity.ok(CompanyMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<ModifyCompanyResponse> modifyCompany(ModifyCompanyRequest modifyCompanyRequest) {
        var company = new ModifyRemoteCompanyRequest(CompanyMapper.INSTANCE.map(modifyCompanyRequest.getCompany()));
        var resp = companyServiceApi.modify(company);
        var response = new ModifyCompanyResponse();
        response.setRc(resp.getRc());
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<PageCompaniesResponse> pageCompanies(PageCompaniesRequest pageCompaniesRequest) {
        var response = companyServiceApi.page(
                PagingMapper.INSTANCE.map(pageCompaniesRequest.getPage()),
                CompanyMapper.INSTANCE.map(pageCompaniesRequest.getSearch()));
        return ResponseEntity.ok(CompanyMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<RemoveOwnerResponse> removeOwner(RemoveOwnerRequest removeOwnerRequest)  {
        var resp = companyServiceApi.removeOwner(removeOwnerRequest.getCompanyId(),removeOwnerRequest.getCustomerId());
        var response = new RemoveOwnerResponse();
        response.setRc(resp.getRc());
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<DeleteCompanyResponse> deleteCompany(DeleteCompanyRequest deleteCompanyRequest) throws Exception {
         companyServiceApi.deleteCompany(deleteCompanyRequest.getId());
         var response = new DeleteCompanyResponse();
         response.setRc(0);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<AssignOwnersResponse> assignOwners(AssignOwnersRequest assignOwnersRequest) {
        var resp = companyServiceApi.assignOwners(assignOwnersRequest.getCompanyId(),assignOwnersRequest.getCustomerIds());
        var response = new AssignOwnersResponse();
        response.setRc(resp.getRc());
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<GetByCompanyIdResponse> getCustomersById(GetByCompanyIdRequest getByCompanyIdRequest)  {
        var getResponse = companyServiceApi.getByCompanyId(getByCompanyIdRequest.getCompId());
        return ResponseEntity.ok(CompanyMapper.INSTANCE.map(getResponse));
    }

    @Override
    public ResponseEntity<GetStoreIncomesResponse> getStoreIncomes(GetStoreIncomesRequest request) {
        var getResponse = companyServiceApi.getStoreIncomes(request.getStoreIds(),request.getDays());
        return ResponseEntity.ok(CompanyMapper.INSTANCE.map(getResponse));
    }

    @Override
    public ResponseEntity<GetStoreReportsResponse> getStoreReports(GetStoreReportsRequest request) {
        var getResponse = companyServiceApi.getStoreReports(request.getStoreIds().get(0),request.getDays());
        return ResponseEntity.ok(CompanyMapper.INSTANCE.maptoStoreRep(getResponse));
    }
}
