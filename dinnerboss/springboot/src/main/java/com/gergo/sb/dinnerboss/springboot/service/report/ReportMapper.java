package com.gergo.sb.dinnerboss.springboot.service.report;

import com.gergo.sb.dinnerboss.serviceapi.service.report.CreateReportDO;
import com.gergo.sb.dinnerboss.serviceapi.service.report.PageRemoteReportResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.report.ReportSearchParams;
import com.gergo.sb.dinnerboss.springboot.model.CreateReportDTO;
import com.gergo.sb.dinnerboss.springboot.model.PageReportResponse;
import com.gergo.sb.dinnerboss.springboot.model.PageReportSearch;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ReportMapper {
    ReportMapper INSTANCE = Mappers.getMapper(ReportMapper.class);

    CreateReportDTO map(CreateReportDO createReportDO);
    CreateReportDO map(CreateReportDTO createReportDO);

    ReportSearchParams map(PageReportSearch pageReportSearch);
    PageReportResponse map(PageRemoteReportResponse pageRemoteReportResponse);

}
