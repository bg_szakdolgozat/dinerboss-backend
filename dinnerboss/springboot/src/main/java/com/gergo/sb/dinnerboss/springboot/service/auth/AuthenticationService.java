package com.gergo.sb.dinnerboss.springboot.service.auth;

import com.gergo.sb.dinnerboss.serviceapi.service.auth.AuthServiceApi;
import com.gergo.sb.dinnerboss.springboot.api.SuperadminApiDelegate;
import com.gergo.sb.dinnerboss.springboot.model.*;
import org.springframework.http.ResponseEntity;

public class AuthenticationService implements SuperadminApiDelegate {

    private AuthServiceApi authServiceApi;

    public AuthenticationService(AuthServiceApi adminServiceApi) {
        this.authServiceApi = adminServiceApi;
    }

    @Override
    public ResponseEntity<LoginAdminResponse> loginSuperadmin(LoginAdminRequest request) {
        var admin = authServiceApi.loginSuperadmin(request.getAdminEmail(),request.getPassword());
        return ResponseEntity.ok(AuthMapper.INSTANCE.map(admin));
    }
}
