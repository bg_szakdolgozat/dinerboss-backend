package com.gergo.sb.dinnerboss.springboot.security.credentials;

import org.springframework.util.Assert;

import java.math.BigInteger;

public class HotpToken {
    private final BigInteger value;

    public HotpToken(BigInteger value) {
        Assert.notNull(value, "HotpToken cannot be null");
        this.value = value;
    }

    public BigInteger getValue() {
        return value;
    }
}
