package com.gergo.sb.dinnerboss.springboot.service.checkout;

import com.gergo.sb.dinnerboss.serviceapi.service.checkout.CheckoutServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.checkout.CreateRemoteCheckoutRequest;
import com.gergo.sb.dinnerboss.springboot.api.CheckoutApiDelegate;
import com.gergo.sb.dinnerboss.springboot.model.*;
import com.gergo.sb.dinnerboss.springboot.service.common.PagingMapper;
import org.springframework.http.ResponseEntity;

public class CheckoutService implements CheckoutApiDelegate {

    private CheckoutServiceApi checkoutServiceApi;

    public CheckoutService(CheckoutServiceApi checkoutServiceApi) {
        this.checkoutServiceApi = checkoutServiceApi;
    }

    @Override
    public ResponseEntity<CreateCheckoutResponse> createCheckout(CreateCheckoutRequest request) {
        var checkout = new CreateRemoteCheckoutRequest(CheckoutMapper.INSTANCE.map(request.getCheckout()));
        var response = checkoutServiceApi.createCommodity(checkout);
        var createResponse = new CreateCheckoutResponse();
        createResponse.setRc(response.getRc());
        return ResponseEntity.ok(createResponse);
    }

    @Override
    public ResponseEntity<PageCheckoutResponse> pageCheckout(PageCheckoutRequest pageCheckoutRequest) {
        var response = checkoutServiceApi.page(
                PagingMapper.INSTANCE.map(pageCheckoutRequest.getPage()),
                CheckoutMapper.INSTANCE.map(pageCheckoutRequest.getSearch()));
        return ResponseEntity.ok(CheckoutMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<DeleteCheckoutResponse> deleteCheckout(DeleteCheckoutRequest deleteCheckoutRequest) {
        checkoutServiceApi.deleteCheckout(deleteCheckoutRequest.getId());
        var response = new DeleteCheckoutResponse();
        response.setRc(0);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<ModifyCheckoutResponse> modifyCheckout(ModifyCheckoutRequest modifyCheckoutRequest) {
        var remoteCheckout = CheckoutMapper.INSTANCE.map(modifyCheckoutRequest.getCheckout());
        var remoteResponse = checkoutServiceApi.modify(remoteCheckout);
        var response = new ModifyCheckoutResponse();
        response.setRc(remoteResponse.getRc());
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<AddTransmissionResponse> addTransmission(AddTransmissionRequest addTransmissionRequest)  {
        var checkout =  CheckoutMapper.INSTANCE.map(addTransmissionRequest.getTransmission());
        var response = checkoutServiceApi.addTransmission(checkout);
        var createResponse = new AddTransmissionResponse();
        createResponse.setRc(response.getRc());
        return ResponseEntity.ok(createResponse);
    }

    @Override
    public ResponseEntity<PageTransmissionsResponse> pageTransmissions(PageTransmissionsRequest pageTransmissionsRequest) {
        var response = checkoutServiceApi.pageTransmissions(
                PagingMapper.INSTANCE.map(pageTransmissionsRequest.getPage()),
                CheckoutMapper.INSTANCE.map(pageTransmissionsRequest.getSearch()));
        return ResponseEntity.ok(CheckoutMapper.INSTANCE.map(response));
    }
}
