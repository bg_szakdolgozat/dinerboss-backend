package com.gergo.sb.dinnerboss.springboot.service.store;

import com.gergo.sb.dinnerboss.serviceapi.service.company.GetOwnersByIdResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.store.*;
import com.gergo.sb.dinnerboss.springboot.model.*;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StoreMapper {
    StoreMapper INSTANCE = Mappers.getMapper(StoreMapper.class);

    StoreDTO map(StoreDO storeDO);
    StoreDO map(StoreDTO storeDO);

    CreateStoreDTO map(CreateStoreDO createStoreDO);
    CreateStoreDO map( CreateStoreDTO createStoreDTO);

    CreateStoreRequest map(CreateRemoteStoreRequest request);
    StoreSearchParams map(PageStoreSearch pageStoreSearch);

    PageStoresResponse map(PageRemoteStoreResponse response);
    GetStoreByCompIdResponse map(GetRemoteStoresByCompIdResponse response);
    GetByStoreIdResponse map(GetCustomersByIdResponse response);
    CustomerProfileDTO map(CustomerProfileDO domainDO);



    CustomerProfileDO map(CustomerProfileDTO domainDO);
}
