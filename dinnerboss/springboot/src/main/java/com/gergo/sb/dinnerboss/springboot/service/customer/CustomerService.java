package com.gergo.sb.dinnerboss.springboot.service.customer;

import com.gergo.sb.dinnerboss.serviceapi.service.customer.CreateRemoteCustomerRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.ModifyRemoteCustomerRequest;
import com.gergo.sb.dinnerboss.springboot.api.CustomerApiDelegate;
import com.gergo.sb.dinnerboss.springboot.model.*;
import com.gergo.sb.dinnerboss.springboot.service.common.PagingMapper;
import org.springframework.http.ResponseEntity;


public class CustomerService implements CustomerApiDelegate {

    private CustomerServiceApi customerServiceApi;

    public CustomerService(CustomerServiceApi customerServiceApi) {
        this.customerServiceApi = customerServiceApi;
    }

    @Override
    public ResponseEntity<CreateCustomerResponse> createCustomer(CreateCustomerRequest request) {
        var admin = new CreateRemoteCustomerRequest(CustomerMapper.INSTANCE.map(request.getCustomer()));
        var response = customerServiceApi.createCustomer(admin);
        return ResponseEntity.ok(CustomerMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<GetCustomerByEmailResponse> getCustomerByEmail(GetCustomerByEmailRequest request) {
        var response = customerServiceApi.getCustomerByEmail(request.getEmail());
        return ResponseEntity.ok(CustomerMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<PageCustomersResponse> pageCustomers(PageCustomersRequest pageCustomersRequest){
        var response = customerServiceApi.page(
                PagingMapper.INSTANCE.map(pageCustomersRequest.getPage()),
                CustomerMapper.INSTANCE.map(pageCustomersRequest.getSearch()));
        return ResponseEntity.ok(CustomerMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<ModifyCustomerResponse> modifyCustomer(ModifyCustomerRequest modifyCustomerRequest) {
        var admin = new ModifyRemoteCustomerRequest(CustomerMapper.INSTANCE.map(modifyCustomerRequest.getCustomer()));
        var resp = customerServiceApi.modify(admin);
        var response = new ModifyCustomerResponse();
        response.setRc(resp.getRc());
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<DeleteCustomerResponse> deleteCustomer(DeleteCustomerRequest deleteCustomerRequest) throws Exception {
        customerServiceApi.deleteCustomer(deleteCustomerRequest.getId());
        var response = new DeleteCustomerResponse();
        response.setRc(0);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<LoginCustomerResponse> loginCustomer(LoginCustomerRequest request) {
        var admin = customerServiceApi.loginAdmin(request.getEmail(),request.getPassword());
        return ResponseEntity.ok(CustomerMapper.INSTANCE.map(admin));
    }
}
