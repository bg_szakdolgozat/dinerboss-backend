package com.gergo.sb.dinnerboss.springboot.service;


import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.gergo.sb.dinnerboss.serviceapi.rc.exception.ReturnCodeAwareException;
import com.gergo.sb.dinnerboss.springboot.model.ResponseBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice(basePackages = "com.gergo.sb.dinnerboss.springboot.api")
public class RestValidationExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(com.gergo.sb.dinnerboss.springboot.service.RestValidationExceptionHandler.class);

    @ExceptionHandler({
            InvalidFormatException.class,
            IllegalStateException.class,
            MethodArgumentNotValidException.class,
            ConstraintViolationException.class,
            HttpMessageNotReadableException.class,
            IllegalArgumentException.class
    })
    protected ResponseEntity<ResponseBase> handleValidation(Exception e) {
        ResponseBase bodyOfResponse = new ResponseBase()
                .rc(100);
        logger.error("Validation error: " + e.getMessage());
        return ResponseEntity.badRequest().body(bodyOfResponse);
    }

    @ExceptionHandler()
    protected ResponseEntity<ResponseBase> handleReturnCodeException(RuntimeException e) {
        if(e instanceof ReturnCodeAwareException){
            ReturnCodeAwareException exception = (ReturnCodeAwareException) e;
            ResponseBase resp = new ResponseBase()
                    .rc(exception.getRc());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
        }
        ResponseBase resp = new ResponseBase()
                .rc(900);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);

    }
}
