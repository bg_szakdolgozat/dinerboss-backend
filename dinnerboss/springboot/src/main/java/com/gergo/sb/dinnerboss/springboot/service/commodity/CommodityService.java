package com.gergo.sb.dinnerboss.springboot.service.commodity;

import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommodityServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CreateRemoteCommodityRequest;
import com.gergo.sb.dinnerboss.springboot.api.CommodityApiDelegate;
import com.gergo.sb.dinnerboss.springboot.model.*;
import com.gergo.sb.dinnerboss.springboot.service.common.PagingMapper;
import org.springframework.http.ResponseEntity;

public class CommodityService implements CommodityApiDelegate {

    private CommodityServiceApi commodityServiceApi;

    public CommodityService(CommodityServiceApi commodityServiceApi) {
        this.commodityServiceApi = commodityServiceApi;
    }

    @Override
    public ResponseEntity<CreateCommodityResponse> createCommodity(CreateCommodityRequest request) {
        var commodity = new CreateRemoteCommodityRequest(CommodityMapper.INSTANCE.map(request.getCommodity()));
        var response = commodityServiceApi.createCommodity(commodity);
        var createResponse = new CreateCommodityResponse();
        createResponse.setRc(response.getRc());
        return ResponseEntity.ok(createResponse);
    }

    @Override
    public ResponseEntity<PageCommodityResponse> pageCommodity(PageCommodityRequest pageCommodityRequest) {
        var response = commodityServiceApi.page(
                PagingMapper.INSTANCE.map(pageCommodityRequest.getPage()),
                CommodityMapper.INSTANCE.map(pageCommodityRequest.getSearch()));
        return ResponseEntity.ok(CommodityMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<DeleteCommodityResponse> deleteCommodity(DeleteCommodityRequest deleteCommodityRequest) {
        commodityServiceApi.deleteStore(deleteCommodityRequest.getId());
        var response = new DeleteCommodityResponse();
        response.setRc(0);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<ModifyCommodityResponse> modifyCommodity(ModifyCommodityRequest modifyCommodityRequest) {
        var remoteCommodity = CommodityMapper.INSTANCE.map(modifyCommodityRequest.getCommodity());
        var remoteResponse = commodityServiceApi.modify(remoteCommodity);
        var response = new ModifyCommodityResponse();
        response.setRc(remoteResponse.getRc());
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<CreateCommodityResponse> createTool(CreateCommodityRequest createCommodityRequest) {
        var tool = new CreateRemoteCommodityRequest(CommodityMapper.INSTANCE.map(createCommodityRequest.getCommodity()));
        var response = commodityServiceApi.createTool(tool);
        var createResponse = new CreateCommodityResponse();
        createResponse.setRc(response.getRc());
        return ResponseEntity.ok(createResponse);
    }

    @Override
    public ResponseEntity<PageCommodityResponse> pageTools(PageCommodityRequest pageCommodityRequest) {
        var response = commodityServiceApi.pageTools(
                PagingMapper.INSTANCE.map(pageCommodityRequest.getPage()),
                CommodityMapper.INSTANCE.map(pageCommodityRequest.getSearch()));
        return ResponseEntity.ok(CommodityMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<DeleteCommodityResponse> deleteTool(DeleteCommodityRequest deleteCommodityRequest){
        commodityServiceApi.deleteTool(deleteCommodityRequest.getId());
        var response = new DeleteCommodityResponse();
        response.setRc(0);
        return ResponseEntity.ok(response);
    }
}
