package com.gergo.sb.dinnerboss.springboot.service.auth;


import com.gergo.sb.dinnerboss.serviceapi.service.admin.CreateAdminDO;

import java.util.Optional;

public interface AdminAuthenticator {
    Optional<CreateAdminDO> authenticate(String adminId, String password);

}
