package com.gergo.sb.dinnerboss.springboot.security.authentication;


import com.gergo.sb.dinnerboss.springboot.security.credentials.Email;
import com.gergo.sb.dinnerboss.springboot.security.credentials.Password;
import org.springframework.util.Assert;

import java.util.Base64;

class PasswordTokenImpl implements PasswordToken {
	private Email email;
	private Password password;

	PasswordTokenImpl(String authorizationToken) {
		Assert.notNull(authorizationToken, "Admin Authorization token cannot be null");
		String base64Token = authorizationToken.split("Basic ")[1];
		String token = new String(Base64.getDecoder().decode(base64Token));
		String[] parts = token.split(":");
		Assert.isTrue(parts.length == 2, "Authorization token must 2 parts separated by colon");
		this.email = new Email(parts[0]);
		this.password = new Password(parts[1]);
	}


	@Override
	public Email getEmail() {
		return email;
	}

	@Override
	public Password getPassword() {
		return password;
	}
}
