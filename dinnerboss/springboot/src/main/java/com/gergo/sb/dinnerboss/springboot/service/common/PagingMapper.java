package com.gergo.sb.dinnerboss.springboot.service.common;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.springboot.model.PageData;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PagingMapper {
	PagingMapper INSTANCE = Mappers.getMapper(PagingMapper.class);

	@Mapping(source = "sortOrder.orderBy", target = "orderBy")
	@Mapping(source = "sortOrder.direction", target = "direction")
	PagedRequest map(PageData pageData);
}
