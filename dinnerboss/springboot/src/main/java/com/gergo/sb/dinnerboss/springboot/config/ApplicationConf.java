package com.gergo.sb.dinnerboss.springboot.config;


import com.gergo.sb.dinnerboss.repository.admin.AdminDaoImpl;
import com.gergo.sb.dinnerboss.repository.admin.AdminDaoMapper;
import com.gergo.sb.dinnerboss.repository.admin.AdminSessionDaoImpl;
import com.gergo.sb.dinnerboss.repository.admin.AdminSessionDaoMapper;
import com.gergo.sb.dinnerboss.repository.auth.AuthDaoImpl;
import com.gergo.sb.dinnerboss.repository.auth.AuthDaoMapper;
import com.gergo.sb.dinnerboss.repository.checkout.CheckoutDaoImpl;
import com.gergo.sb.dinnerboss.repository.checkout.CheckoutDaoMapper;
import com.gergo.sb.dinnerboss.repository.commodity.CommodityDaoImpl;
import com.gergo.sb.dinnerboss.repository.commodity.CommodityDaoMapper;
import com.gergo.sb.dinnerboss.repository.company.CompanyDaoImpl;
import com.gergo.sb.dinnerboss.repository.company.CompanyDaoMapper;
import com.gergo.sb.dinnerboss.repository.customer.CustomerDaoImpl;
import com.gergo.sb.dinnerboss.repository.customer.CustomerDaoMapper;
import com.gergo.sb.dinnerboss.repository.report.ReportDaoImpl;
import com.gergo.sb.dinnerboss.repository.report.ReportDaoMapper;
import com.gergo.sb.dinnerboss.repository.storage.StorageDaoImpl;
import com.gergo.sb.dinnerboss.repository.storage.StorageDaoMapper;
import com.gergo.sb.dinnerboss.repository.store.StoreDaoImpl;
import com.gergo.sb.dinnerboss.repository.store.StoreDaoMapper;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.AdminServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.auth.AuthServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.checkout.CheckoutServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommodityServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.company.CompanyServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.report.ReportServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.storage.StorageServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreServiceApi;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.dao.AdminDao;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.dao.AdminServiceApiImpl;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.auth.dao.AuthDao;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.auth.dao.AuthServiceApiImpl;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.checkout.CheckoutDao;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.checkout.CheckoutServiceApiImpl;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.commodity.CommodityDao;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.commodity.CommodityServiceApiImpl;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.company.CompanyDao;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.company.CompanyServiceApiImpl;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.customer.CustomerServiceApiImpl;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.customer.dao.CustomerDao;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.report.ReportDao;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.report.ReportServiceApiImpl;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.storage.StorageDao;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.storage.StorageServiceApiImpl;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.store.StoreDao;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.store.StoreServiceApiImpl;
import com.gergo.sb.dinnerboss.springboot.api.*;
import com.gergo.sb.dinnerboss.springboot.service.AdminService;
import com.gergo.sb.dinnerboss.springboot.service.auth.AuthenticationService;
import com.gergo.sb.dinnerboss.springboot.service.checkout.CheckoutService;
import com.gergo.sb.dinnerboss.springboot.service.commodity.CommodityService;
import com.gergo.sb.dinnerboss.springboot.service.company.CompanyService;
import com.gergo.sb.dinnerboss.springboot.service.customer.CustomerService;
import com.gergo.sb.dinnerboss.springboot.service.report.ReportService;
import com.gergo.sb.dinnerboss.springboot.service.storage.StorageService;
import com.gergo.sb.dinnerboss.springboot.service.store.StoreService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConf {

    /*
        Service
     */
//    @Bean
//    public AdminService adminService(AccountDao accountDao, DeviceDao deviceDao) {
//        return new AccountServiceImpl(accountDao, deviceDao);
//    }

    @Bean
    SuperadminApiDelegate authApiDelegate(AuthServiceApi authServiceApi) {
        return new AuthenticationService(authServiceApi);
    }

    @Bean
    AdminApiDelegate testApiDelegate(AdminServiceApi adminServiceApi) {
        return new AdminService(adminServiceApi);
    }

    @Bean
    CustomerApiDelegate customerApiDelegate(CustomerServiceApi customerServiceApi) {
        return new CustomerService(customerServiceApi);
    }

    @Bean
    CommodityApiDelegate commodityApiDelegate(CommodityServiceApi commodityServiceApi) {
        return new CommodityService(commodityServiceApi);
    }

    @Bean
    CompanyApiDelegate companyApiDelegate(CompanyServiceApi companyServiceApi) {
        return new CompanyService(companyServiceApi);
    }

    @Bean
    CheckoutApiDelegate checkoutApiDelegate(CheckoutServiceApi checkoutServiceApi) {
        return new CheckoutService(checkoutServiceApi);
    }

    @Bean
    StoreApiDelegate storeApiDelegate(StoreServiceApi storeServiceApi) {
        return new StoreService(storeServiceApi);
    }

    @Bean
    StorageApiDelegate storageApiDelegate(StorageServiceApi storageServiceApi) {
        return new StorageService(storageServiceApi);
    }
    @Bean
    ReportApiDelegate reportApiDelegate(ReportServiceApi reportServiceApi) {
        return new ReportService(reportServiceApi);
    }

    /*
        Dao
     */
    @Bean
    public AdminDao adminDao(AdminDaoMapper adminDaoMapper) {
        return new AdminDaoImpl(adminDaoMapper);
    }

    @Bean
    public CustomerDao customerDao(CustomerDaoMapper customerDaoMapper, StoreDaoMapper storeDaoMapper) {
        return new CustomerDaoImpl(customerDaoMapper, storeDaoMapper);
    }

    @Bean
    public CompanyDao companyDao(CompanyDaoMapper companyDaoMapper,StoreDaoMapper storeDaoMapper) {
        return new CompanyDaoImpl(companyDaoMapper, storeDaoMapper);
    }

    @Bean
    public StoreDao storeDao(StoreDaoMapper storeDaoMapper) {
        return new StoreDaoImpl(storeDaoMapper);
    }

    @Bean
    public CommodityDao commodityDao(CommodityDaoMapper commodityDaoMapper) {
        return new CommodityDaoImpl(commodityDaoMapper);
    }

    @Bean
    public CheckoutDao checkoutDao(CheckoutDaoMapper checkoutDaoMapper) {
        return new CheckoutDaoImpl(checkoutDaoMapper);
    }

    @Bean
    public StorageDao storageDao(StorageDaoMapper storageDaoMapper) {
        return new StorageDaoImpl(storageDaoMapper);
    }

    @Bean
    public ReportDao reportDao(ReportDaoMapper reportDaoMapper) {
        return new ReportDaoImpl(reportDaoMapper);
    }

    @Bean
    public AuthDao authDao(AuthDaoMapper authDaoMapper) {
        return new AuthDaoImpl(authDaoMapper);
    }

    @Bean
    AdminSessionDaoImpl sessionDao(AdminSessionDaoMapper adminSessionDaoMapper) {
        return new AdminSessionDaoImpl(adminSessionDaoMapper);
    }


    /*
        End of dao
     */
    @Bean
    AdminServiceApi adminServiceApi(AdminDao adminDao) {
        return new AdminServiceApiImpl(adminDao);
    }

    @Bean
    CustomerServiceApi customerServiceApi(CustomerDao customerDao) {
        return new CustomerServiceApiImpl(customerDao);
    }

    @Bean
    CommodityServiceApi commodityServiceApi(CommodityDao commodityDao) {
        return new CommodityServiceApiImpl(commodityDao);
    }

    @Bean
    CompanyServiceApi companyServiceApi(CompanyDao companyDao) {
        return new CompanyServiceApiImpl(companyDao);
    }

    @Bean
    StoreServiceApi storeServiceApi(StoreDao storeDao) {
        return new StoreServiceApiImpl(storeDao);
    }

    @Bean
    AuthServiceApi authServiceApi(AuthDao authDao) {
        return new AuthServiceApiImpl(authDao);
    }

    @Bean
    StorageServiceApi storageServiceApi(StorageDao storageDao,StoreDao storeDao) {
        return new StorageServiceApiImpl(storageDao,storeDao);
    }

    @Bean
    CheckoutServiceApi checkoutServiceApi(CheckoutDao checkoutDao,StoreDao storeDao,CustomerDao customerDao) {
        return new CheckoutServiceApiImpl(checkoutDao,storeDao,customerDao);
    }
    @Bean
    ReportServiceApi reportServiceApi(ReportDao reportDao, CustomerDao customerDao, StoreDao storeDao) {
        return new ReportServiceApiImpl(reportDao,storeDao,customerDao);
    }
}
