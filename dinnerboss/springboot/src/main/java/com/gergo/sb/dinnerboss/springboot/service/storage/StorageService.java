package com.gergo.sb.dinnerboss.springboot.service.storage;

import com.gergo.sb.dinnerboss.serviceapi.service.storage.StorageServiceApi;
import com.gergo.sb.dinnerboss.springboot.api.StorageApiDelegate;
import com.gergo.sb.dinnerboss.springboot.model.*;
import com.gergo.sb.dinnerboss.springboot.service.common.PagingMapper;
import org.springframework.http.ResponseEntity;

public class StorageService implements StorageApiDelegate {

    private StorageServiceApi storageServiceApi;

    public StorageService(StorageServiceApi storageServiceApi) {
        this.storageServiceApi = storageServiceApi;
    }

    @Override
    public ResponseEntity<CreateStorageResponse> createStorage(CreateStorageRequest request) {
        var response = storageServiceApi.createStorage(request.getStoreId());
        var createResponse = new CreateStorageResponse();
        createResponse.setRc(response.getRc());
        return ResponseEntity.ok(createResponse);
    }

    @Override
    public ResponseEntity<DeleteStorageResponse> deleteStorage(DeleteStorageRequest deleteStorageRequest) {
        storageServiceApi.deleteStorage(deleteStorageRequest.getId());
        var response = new DeleteStorageResponse();
        response.setRc(0);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<RegisterCommodityResponse> registerCommodity(RegisterCommodityRequest registerCommodityRequest) {
        var getResponse = storageServiceApi.registerCommodity(StorageMapper.INSTANCE.map(registerCommodityRequest.getRegistration()));
        var response = new RegisterCommodityResponse();
        response.setRc(getResponse.getRc());
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<RegisterToolResponse> registerTool(RegisterToolRequest registerToolRequest) {
        var getResponse = storageServiceApi.registerTool(StorageMapper.INSTANCE.map(registerToolRequest.getRegistration()));
        var response = new RegisterToolResponse();
        response.setRc(getResponse.getRc());
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<PageCommRegistrationsResponse> pageCommRegistrations(PageCommRegistrationsRequest pageCommRegistrationsRequest) {
        var response = storageServiceApi.page(
                PagingMapper.INSTANCE.map(pageCommRegistrationsRequest.getPage()),
                StorageMapper.INSTANCE.map(pageCommRegistrationsRequest.getSearch()));
        return ResponseEntity.ok(StorageMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<PageToolRegistrationsResponse> pageToolRegistrations(PageToolRegistrationsRequest pageToolRegistrationsRequest) {
        var response = storageServiceApi.pageToolReg(
                PagingMapper.INSTANCE.map(pageToolRegistrationsRequest.getPage()),
                StorageMapper.INSTANCE.map(pageToolRegistrationsRequest.getSearch()));
        return ResponseEntity.ok(StorageMapper.INSTANCE.mapToTool(response));
    }

    @Override
    public ResponseEntity<ListCommodityByStorageResponse> listCommodityByStorage(ListCommodityByStorageRequest listCommodityByStorageRequest) {
        var response = storageServiceApi.listCommodityByStorage(listCommodityByStorageRequest.getStorageId());
        return ResponseEntity.ok(StorageMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<ListCommodityByStorageResponse> listToolByStorage(ListToolByStorageRequest listToolByStorageRequest) throws Exception {
        var response = storageServiceApi.listToolByStorage(listToolByStorageRequest.getStorageId());
        return ResponseEntity.ok(StorageMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<GetAllByStoreResponse> getAllByStore(GetAllByStoreRequest getAllByStoreRequest) {
        var response = storageServiceApi.getAllByStore(getAllByStoreRequest.getStoreIds());
        return ResponseEntity.ok(StorageMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<GetConsumableCommoditiesResponse> getConsumableCommodities(GetConsumableCommoditiesRequest getConsumableCommoditiesRequest) {
        var response = storageServiceApi.getConsumableCommodities(getConsumableCommoditiesRequest.getStorageId(),getConsumableCommoditiesRequest.getLimit());
        return ResponseEntity.ok(StorageMapper.INSTANCE.mapToConsumable(response));
    }

    @Override
    public ResponseEntity<GetStorageByIdResponse> getStorageById(GetStorageByIdRequest getStorageByIdRequest)  {
        var resp = storageServiceApi.getById(getStorageByIdRequest.getId());
        var response = new GetStorageByIdResponse();
        response.setRc(0);
        response.setStore(StorageMapper.INSTANCE.map(resp));
        return ResponseEntity.ok(response);
    }
}
