package com.gergo.sb.dinnerboss.springboot.service.store;

import com.gergo.sb.dinnerboss.serviceapi.service.store.CreateRemoteStoreRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreServiceApi;
import com.gergo.sb.dinnerboss.springboot.api.StoreApiDelegate;
import com.gergo.sb.dinnerboss.springboot.model.*;
import com.gergo.sb.dinnerboss.springboot.service.common.PagingMapper;
import com.gergo.sb.dinnerboss.springboot.service.company.CompanyMapper;
import org.springframework.http.ResponseEntity;

public class StoreService implements StoreApiDelegate {

    private StoreServiceApi storeServiceApi;

    public StoreService(StoreServiceApi storeServiceApi) {
        this.storeServiceApi = storeServiceApi;
    }

    @Override
    public ResponseEntity<CreateStoreResponse> createStore(CreateStoreRequest request) throws Exception {
        var store = new CreateRemoteStoreRequest(StoreMapper.INSTANCE.map(request.getStore()));
        var remoteResponse = storeServiceApi.createStore(store);
        var response = new CreateStoreResponse();
        response.setRc(remoteResponse.getRc());
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<DeleteStoreResponse> deleteStore(DeleteStoreRequest deleteStoreRequest) throws Exception {
        storeServiceApi.deleteStore(deleteStoreRequest.getId());
        var response = new DeleteStoreResponse();
        response.setRc(0);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<GetStoreByIdResponse> getStoreById(GetStoreByIdRequest request) throws Exception {
        var getResponse = storeServiceApi.getStoreById(request.getId());
        var response = new GetStoreByIdResponse();
        response.setStore(StoreMapper.INSTANCE.map(getResponse));
        response.setRc(0);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<ModifyStoreResponse> modifyStore(ModifyStoreRequest modifyStoreRequest) throws Exception {
        var remoteStore = StoreMapper.INSTANCE.map(modifyStoreRequest.getStore());
        var remoteResponse = storeServiceApi.modify(remoteStore);
        var response = new ModifyStoreResponse();
        response.setRc(remoteResponse.getRc());
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<PageStoresResponse> pageStores(PageStoresRequest pageStoresRequest) {
        var response = storeServiceApi.page(
                PagingMapper.INSTANCE.map(pageStoresRequest.getPage()),
                StoreMapper.INSTANCE.map(pageStoresRequest.getSearch()));
        return ResponseEntity.ok(StoreMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<GetStoreByCompIdResponse> getByCompanyId(GetStoreByCompIdRequest request) {
        var getResponse = storeServiceApi.getStoresByCompId(request.getId());
        return ResponseEntity.ok(StoreMapper.INSTANCE.map(getResponse));
    }

    @Override
    public ResponseEntity<AssignUsersToStoreResponse> assignUsers(AssignUsersToStoreRequest assignUsersToStoreRequest)  {
        var  remoteResp = storeServiceApi.assignUsers(assignUsersToStoreRequest.getStoreId(),assignUsersToStoreRequest.getUserIds());
        var response = new AssignUsersToStoreResponse();
        response.setRc(remoteResp.getRc());
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<RevokeUsersFromStoreResponse> revokeUsers(RevokeUsersFromStoreRequest revokeUsersFromStoreRequest) {
        var  remoteResp = storeServiceApi.revokeUsers(revokeUsersFromStoreRequest.getStoreId(),revokeUsersFromStoreRequest.getUserIds());
        var response = new RevokeUsersFromStoreResponse();
        response.setRc(remoteResp.getRc());
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<GetByStoreIdResponse> getCustomersById(GetByStoreIdRequest getByStoreIdRequest) throws Exception {
        var getResponse = storeServiceApi.getByStoreId(getByStoreIdRequest.getStoreId());
        return ResponseEntity.ok(StoreMapper.INSTANCE.map(getResponse));
    }
}
