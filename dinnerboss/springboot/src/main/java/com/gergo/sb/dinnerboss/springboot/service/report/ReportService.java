package com.gergo.sb.dinnerboss.springboot.service.report;

import com.gergo.sb.dinnerboss.serviceapi.service.report.CreateRemoteReportRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.report.ReportServiceApi;
import com.gergo.sb.dinnerboss.springboot.api.ReportApiDelegate;
import com.gergo.sb.dinnerboss.springboot.model.CreateReportRequest;
import com.gergo.sb.dinnerboss.springboot.model.CreateReportResponse;
import com.gergo.sb.dinnerboss.springboot.model.PageReportRequest;
import com.gergo.sb.dinnerboss.springboot.model.PageReportResponse;
import com.gergo.sb.dinnerboss.springboot.service.checkout.CheckoutMapper;
import com.gergo.sb.dinnerboss.springboot.service.common.PagingMapper;
import org.springframework.http.ResponseEntity;

public class ReportService implements ReportApiDelegate {

    private ReportServiceApi reportServiceApi;

    public ReportService(ReportServiceApi reportServiceApi) {
        this.reportServiceApi = reportServiceApi;
    }

    @Override
    public ResponseEntity<CreateReportResponse> createReport(CreateReportRequest createReportRequest) {
        var report = new CreateRemoteReportRequest(ReportMapper.INSTANCE.map(createReportRequest.getReport()));
        var response = reportServiceApi.createReport(report);
        var createResponse = new CreateReportResponse();
        createResponse.setRc(response.getRc());
        return ResponseEntity.ok(createResponse);
    }

    @Override
    public ResponseEntity<PageReportResponse> pageReport(PageReportRequest pageReportRequest){
        var response = reportServiceApi.page(
                PagingMapper.INSTANCE.map(pageReportRequest.getPage()),
                ReportMapper.INSTANCE.map(pageReportRequest.getSearch()));
        return ResponseEntity.ok(ReportMapper.INSTANCE.map(response));
    }
}
