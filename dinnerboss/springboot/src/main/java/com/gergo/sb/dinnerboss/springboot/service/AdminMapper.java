package com.gergo.sb.dinnerboss.springboot.service;

import com.gergo.sb.dinnerboss.serviceapi.service.admin.CreateAdminDO;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.CreateRemoteAdminResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.GetRemoteAdminResponse;
import com.gergo.sb.dinnerboss.springboot.model.CreateAdminDTO;
import com.gergo.sb.dinnerboss.springboot.model.CreateAdminResponse;
import com.gergo.sb.dinnerboss.springboot.model.GetAdminResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
@Mapper
public interface AdminMapper {
    AdminMapper INSTANCE = Mappers.getMapper(AdminMapper.class);

    CreateAdminDO map(CreateAdminDTO createAdminDTO);
    CreateAdminResponse map(CreateRemoteAdminResponse createRemoteAdminResponse);

    GetAdminResponse map(GetRemoteAdminResponse getRemoteAdminResponse);
}
