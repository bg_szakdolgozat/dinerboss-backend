package com.gergo.sb.dinnerboss.springboot.security.filter;


import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

public class AuthenticationFilter extends GenericFilterBean {
	private AuthenticationManager authenticationManager;

	public AuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
	private static AntPathRequestMatcher loginMatcher = new AntPathRequestMatcher("/authentication");

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//		try {
//			HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
//			if (httpServletRequest.getRequestURI().contains("adminapi/")) {
//				Authentication authentication = parseAuthenticationToken(httpServletRequest);
//				tryAuthenticate(authentication);
//				updateCookie(httpServletRequest, (HttpServletResponse) servletResponse);
//			}
//			filterChain.doFilter(httpServletRequest, servletResponse);
//		} finally {
//			SecurityContextHolder.clearContext();
//		}
	}

	public static void setLoginMatcher(String path) {
		AuthenticationFilter.loginMatcher = new AntPathRequestMatcher(path);
	}
	private void tryAuthenticate(Authentication authenticationToken) {
		Authentication authentication = authenticationManager.authenticate(authenticationToken);
		if (authentication == null) {
			throw new BadCredentialsException("Bad credentials");
		}
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}


}
