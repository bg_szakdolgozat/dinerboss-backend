package com.gergo.sb.dinnerboss.springboot.service.storage;

import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommodityDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.storage.*;
import com.gergo.sb.dinnerboss.springboot.model.*;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StorageMapper {
    StorageMapper INSTANCE = Mappers.getMapper(StorageMapper.class);



    RegistrationDTO map(RegisterDO registerDO);

    RegisterDO map(RegistrationDTO registrationDTO);

    PageRemoteCommRegistrationsSearch map(PageCommRegistrationsSearch pageCommRegistrationsSearch);
    PageRemoteCommRegistrationsSearch map(PageToolRegistrationsSearch pageCommRegistrationsSearch);

    PageCommRegistrationsResponse map(PageRemoteCommRegistrationResponse response);
    PageToolRegistrationsResponse mapToTool(PageRemoteCommRegistrationResponse response);

    RemoteCommodityRegDO map(CommodityRegistrationDTO dto);
    CommodityRegistrationDTO map(RemoteCommodityRegDO dto);

    CommodityDO map(CommodityDTO commodityDTO);
    CommodityDTO map(CommodityDO commodityDO);
    ToolRegistrationDTO maptoToolReg( RemoteCommodityRegDO commodityDO);

    CustomerProfileDO map(CustomerProfileDTO customerProfileDTO);
    CustomerProfileDTO map(CustomerProfileDO customerProfileDO);

    ListCommodityByStorageRemoteResponse map(ListCommodityByStorageResponse listCommodityByStorageResponse);
    ListCommodityByStorageResponse map(ListCommodityByStorageRemoteResponse listCommodityByStorageRemoteResponse);

    GetConsumableCommoditiesResponse mapToConsumable( ListCommodityByStorageRemoteResponse listCommodityByStorageRemoteResponse);

    GetAllByStoreResponse map(GetAllByStoreRemoteResponse getAllByStoreRemoteResponse);

    StorageDO map(StorageDTO storageDTO);
    StorageDTO map(StorageDO storageDTO);




}
