package com.gergo.sb.dinnerboss.springboot.security.authentication;


import com.gergo.sb.dinnerboss.springboot.security.credentials.Email;
import com.gergo.sb.dinnerboss.springboot.security.credentials.Password;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class PasswordAuthentication extends UsernamePasswordAuthenticationToken {

	private Email email;
	private Password password;

	public PasswordAuthentication(Email username, Password password) {
		super(username, password);
		this.email = username;
		this.password = password;
	}

	@Override
	public Email getPrincipal() {
		return email;
	}

	@Override
	public Password getCredentials() {
		return password;
	}
}
