package com.gergo.sb.dinnerboss.springboot.config;

import com.gergo.sb.dinnerboss.repository.admin.AdminDaoMapper;
import com.gergo.sb.dinnerboss.repository.admin.AdminSessionDaoMapper;
import com.gergo.sb.dinnerboss.repository.auth.AuthDaoMapper;
import com.gergo.sb.dinnerboss.repository.checkout.CheckoutDaoMapper;
import com.gergo.sb.dinnerboss.repository.commodity.CommodityDaoMapper;
import com.gergo.sb.dinnerboss.repository.company.CompanyDaoMapper;
import com.gergo.sb.dinnerboss.repository.customer.CustomerDaoMapper;
import com.gergo.sb.dinnerboss.repository.report.ReportDaoMapper;
import com.gergo.sb.dinnerboss.repository.storage.StorageDaoMapper;
import com.gergo.sb.dinnerboss.repository.store.StoreDaoMapper;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;




@Configuration
public class DbConfig {

    /*
        DB config
     */
    @Primary
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSourceProperties dataSourceProperties() {
        return new DataSourceProperties();
    }

    @Primary
    @Bean
    public DataSource dataSource(DataSourceProperties dataSourceProperties) {
        return dataSourceProperties.initializeDataSourceBuilder().build();
    }

    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean(DataSource dataSource) {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        sqlSessionFactoryBean.setConfigLocation(
                new ClassPathResource("com/gergo/sb/dinnerboss/repository/config.xml")
        );
        return sqlSessionFactoryBean;
    }

    @Primary
    @Bean
    public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

    /*
        Dao mapper
     */
    @Bean
    public AdminDaoMapper adminDaoMapperDaoMapper(SqlSessionTemplate sqlSessionTemplate) {
        return sqlSessionTemplate.getMapper(AdminDaoMapper.class);
    }
    @Bean
    public AdminSessionDaoMapper adminSessionDaoMapper(SqlSessionTemplate sqlSessionTemplate) {
        return sqlSessionTemplate.getMapper(AdminSessionDaoMapper.class);
    }
    @Bean
    public AuthDaoMapper authDaoMapper(SqlSessionTemplate sqlSessionTemplate) {
        return sqlSessionTemplate.getMapper(AuthDaoMapper.class);
    }
    @Bean
    public CustomerDaoMapper customerDaoMapper(SqlSessionTemplate sqlSessionTemplate) {
        return sqlSessionTemplate.getMapper(CustomerDaoMapper.class);
    }
    @Bean
    public CompanyDaoMapper companyDaoMapper(SqlSessionTemplate sqlSessionTemplate) {
        return sqlSessionTemplate.getMapper(CompanyDaoMapper.class);
    }

    @Bean
    public StoreDaoMapper storeDaoMapper(SqlSessionTemplate sqlSessionTemplate) {
        return sqlSessionTemplate.getMapper(StoreDaoMapper.class);
    }
    @Bean
    public CommodityDaoMapper commodityDaoMapper(SqlSessionTemplate sqlSessionTemplate) {
        return sqlSessionTemplate.getMapper(CommodityDaoMapper.class);
    }
    @Bean
    public StorageDaoMapper storageDaoMapper(SqlSessionTemplate sqlSessionTemplate) {
        return sqlSessionTemplate.getMapper(StorageDaoMapper.class);
    }
    @Bean
    public CheckoutDaoMapper checkoutDaoMapper(SqlSessionTemplate sqlSessionTemplate) {
        return sqlSessionTemplate.getMapper(CheckoutDaoMapper.class);
    }
    @Bean
    public ReportDaoMapper reportDaoMapper(SqlSessionTemplate sqlSessionTemplate) {
        return sqlSessionTemplate.getMapper(ReportDaoMapper.class);
    }
}
