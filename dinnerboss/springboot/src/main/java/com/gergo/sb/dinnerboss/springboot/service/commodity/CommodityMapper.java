package com.gergo.sb.dinnerboss.springboot.service.commodity;

import com.gergo.sb.dinnerboss.serviceapi.service.checkout.CheckoutDO;
import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommodityDO;
import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommoditySearchParams;
import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CreateCommodityDO;
import com.gergo.sb.dinnerboss.serviceapi.service.commodity.PageRemoteCommodityResponse;
import com.gergo.sb.dinnerboss.springboot.model.*;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommodityMapper {
    CommodityMapper INSTANCE = Mappers.getMapper(CommodityMapper.class);

    CreateCommodityDO map(CreateCommodityDTO createCommodityDTO);
    CommodityDTO map(CommodityDO commodityDO);
    CommodityDO map(CommodityDTO commodityDO);
    CheckoutDO map(CheckoutDTO checkoutDTO);
    CheckoutDTO map(CheckoutDO checkoutDO);
    CommoditySearchParams map(PageCommoditySearch search);

    PageCommodityResponse map(PageRemoteCommodityResponse pageRemoteCompaniesResponse);
}
