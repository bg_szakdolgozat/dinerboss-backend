package com.gergo.sb.dinnerboss.springboot.service.auth;

import com.gergo.sb.dinnerboss.serviceapi.service.admin.CreateAdminDO;
import com.gergo.sb.dinnerboss.serviceapi.service.auth.LoginRemoteAdminResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.auth.RemoteAdminProfile;
import com.gergo.sb.dinnerboss.springboot.model.AdminProfile;
import com.gergo.sb.dinnerboss.springboot.model.CreateAdminDTO;
import com.gergo.sb.dinnerboss.springboot.model.LoginAdminResponse;
import com.gergo.sb.dinnerboss.springboot.service.AdminMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AuthMapper {
    AuthMapper INSTANCE = Mappers.getMapper(AuthMapper.class);

    LoginAdminResponse map(LoginRemoteAdminResponse remoteAdminProfile);


}
