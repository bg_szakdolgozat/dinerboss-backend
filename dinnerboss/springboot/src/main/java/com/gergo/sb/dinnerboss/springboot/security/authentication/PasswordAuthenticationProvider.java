package com.gergo.sb.dinnerboss.springboot.security.authentication;


import com.gergo.sb.dinnerboss.serviceapi.rc.exception.ReturnCodeAwareException;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.authentication.loginaction.CreateAdminSession;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

//NINCS KÉSZ

public class PasswordAuthenticationProvider implements AuthenticationProvider {
    private CreateAdminSession createAdminSession;

    public PasswordAuthenticationProvider(
                                          CreateAdminSession createAdminSession) {
        this.createAdminSession = createAdminSession;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (authentication.getPrincipal() == null || authentication.getCredentials() == null) {
            return null;
        }
        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return false;
    }

    private class BackendCallException extends AuthenticationException {
        BackendCallException(ReturnCodeAwareException e) {
            super(String.valueOf(e.getRc()), e);
        }
    }
}
