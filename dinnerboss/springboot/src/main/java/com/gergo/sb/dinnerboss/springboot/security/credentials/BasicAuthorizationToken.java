package com.gergo.sb.dinnerboss.springboot.security.credentials;

import org.springframework.util.Assert;

import java.util.Base64;

public class BasicAuthorizationToken {
    private static final String DELIMITER = ":";

    private String email;
    private String password;

    public BasicAuthorizationToken(String encodedToken) {
        Assert.notNull(encodedToken, "Token is null");
        String base64Token = encodedToken.split("Basic ")[1];
        byte[] decodedToken = Base64.getDecoder().decode(base64Token);
        String plainText = new String(decodedToken);
        Assert.isTrue(plainText.contains(DELIMITER), "Cannot parse basic authorization token");
        String[] credentials = plainText.split(DELIMITER);
        this.email = credentials[0];
        this.password = credentials[1];
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }
}
