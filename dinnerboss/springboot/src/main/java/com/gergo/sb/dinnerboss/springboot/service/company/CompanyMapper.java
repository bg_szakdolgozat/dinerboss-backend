package com.gergo.sb.dinnerboss.springboot.service.company;

import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.company.*;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerSearchParams;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.PageRemoteCustomerResponse;
import com.gergo.sb.dinnerboss.springboot.model.*;
import com.gergo.sb.dinnerboss.springboot.service.customer.CustomerMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CompanyMapper {
    CompanyMapper INSTANCE = Mappers.getMapper(CompanyMapper.class);

    CreateCompanyDO map(CreateCompanyDTO createCompanyDTO);

    CreateCompanyResponse map(RemoteBaseResponse remoteBaseResponse);

    GetCompanyByNameResponse map(GetRemoteCompanyByName response);
     CompanyDTO map(CompanyDO domainDO);
    CompanyDO map(CompanyDTO companyDTO);
    CompanySearchParams map(PageCompaniesSearch search);

    PageCompaniesResponse map(PageRemoteCompaniesResponse pageRemoteCompaniesResponse);

    GetByCompanyIdResponse map(GetOwnersByIdResponse response);

    CustomerProfileDTO map(CustomerProfileDO domainDO);



    CustomerProfileDO map(CustomerProfileDTO domainDO);

    StoreIncomeDTO map(StoreIncomeDO storeIncomeDO);

    StoreIncomeResponse map ( GetStoreIncomesResponse getStoreIncomesResponse);
    GetStoreIncomesResponse map ( StoreIncomeResponse getStoreIncomesResponse);
    GetStoreReportsResponse maptoStoreRep ( StoreIncomeResponse getStoreIncomesResponse);

    SeriesDO map(SeriesDTO seriesDTO);
    SeriesDTO map(SeriesDO seriesDTO);

}
