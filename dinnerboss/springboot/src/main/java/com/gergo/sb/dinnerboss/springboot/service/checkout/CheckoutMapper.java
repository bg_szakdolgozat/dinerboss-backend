package com.gergo.sb.dinnerboss.springboot.service.checkout;

import com.gergo.sb.dinnerboss.serviceapi.service.checkout.*;
import com.gergo.sb.dinnerboss.springboot.model.*;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CheckoutMapper {
    CheckoutMapper INSTANCE = Mappers.getMapper(CheckoutMapper.class);

    CreateRemoteCheckoutRequest map(CreateCheckoutRequest createCheckoutRequest);

    CreateCheckoutDO map(CreateCheckoutDTO createCheckoutDTO);

    CreateCheckoutDTO map(CreateCheckoutDO createCheckoutDO);

    CheckoutSearchParams map(PageCheckoutSearch search);

    TransmissionSearchParams map(PageTransmissionSearch search);

    PageCheckoutResponse map(PageRemoteCheckoutResponse pageRemoteCheckoutResponse);

    PageTransmissionsResponse map(PageRemoteTransmissionResponse pageRemoteTransmissionResponse);


    CheckoutDTO map(CheckoutDO checkoutDO);

    ModifyCheckoutDO map(ModifyCheckoutDTO modifyCheckoutDTO);

    AddTransmissionDO map(AddTransmissionDTO addTransmissionDTO);
}
