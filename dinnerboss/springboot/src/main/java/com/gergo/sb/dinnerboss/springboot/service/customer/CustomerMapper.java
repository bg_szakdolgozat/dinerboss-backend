package com.gergo.sb.dinnerboss.springboot.service.customer;

import com.gergo.sb.dinnerboss.serviceapi.service.company.CompanyDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.*;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreDO;
import com.gergo.sb.dinnerboss.springboot.model.*;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CustomerMapper {
    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);
    CompanyDTO map (CompanyDO companyDO);
    CompanyDTO map (CompanyDTO companyDO);
    CreateCustomerDO map(CreateCustomerDTO createAdminDTO);
    CreateCustomerResponse map(CreateRemoteCustomerResponse createRemoteAdminResponse);
    GetCustomerByEmailResponse map(GetRemoteCustomerByEmailResponse getRemoteCustomerByEmailResponse);

    CustomerSearchParams map(PageCustomersSearch search);
    CustomerProfileDTO map(CustomerProfileDO domainDO);

    StoreDO map(StoreDTO storeDTO);

    CustomerProfileDO map(CustomerProfileDTO domainDO);

    PageCustomersResponse map(PageRemoteCustomerResponse pageRemoteCustomerResponse);

    LoginCustomerResponse map(LoginCustomerResponseDO loginCustomerResponseDO);

}
