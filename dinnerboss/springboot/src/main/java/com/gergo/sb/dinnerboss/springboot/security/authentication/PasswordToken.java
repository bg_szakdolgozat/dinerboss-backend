package com.gergo.sb.dinnerboss.springboot.security.authentication;

import com.gergo.sb.dinnerboss.springboot.security.credentials.Email;
import com.gergo.sb.dinnerboss.springboot.security.credentials.Password;

public interface PasswordToken {
	Email getEmail();

	Password getPassword();

	static PasswordToken getInstance(String authorizationToken) {
		return new PasswordTokenImpl(authorizationToken);
	}
}
