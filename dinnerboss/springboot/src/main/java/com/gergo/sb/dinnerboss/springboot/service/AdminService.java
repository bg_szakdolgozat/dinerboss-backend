package com.gergo.sb.dinnerboss.springboot.service;

import com.gergo.sb.dinnerboss.serviceapi.service.admin.AdminServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.CreateRequest;
import com.gergo.sb.dinnerboss.springboot.api.AdminApiDelegate;
import com.gergo.sb.dinnerboss.springboot.model.CreateAdminRequest;
import com.gergo.sb.dinnerboss.springboot.model.CreateAdminResponse;

import com.gergo.sb.dinnerboss.springboot.model.GetAdminRequest;
import com.gergo.sb.dinnerboss.springboot.model.GetAdminResponse;
import org.springframework.http.ResponseEntity;

public class AdminService implements AdminApiDelegate {

    private AdminServiceApi adminServiceApi;

    public AdminService(AdminServiceApi adminServiceApi) {
        this.adminServiceApi = adminServiceApi;
    }


    @Override
    public ResponseEntity<CreateAdminResponse> createAdmin(CreateAdminRequest request) {
        var admin = new CreateRequest(AdminMapper.INSTANCE.map(request.getAdmin()));
        var response = adminServiceApi.createAdmin(admin);
        return ResponseEntity.ok(AdminMapper.INSTANCE.map(response));
    }

    @Override
    public ResponseEntity<GetAdminResponse> getAdminByEmail(GetAdminRequest request) {
        var response = adminServiceApi.getAdmin(request.getEmail());
        return ResponseEntity.ok(AdminMapper.INSTANCE.map(response));
    }

}
