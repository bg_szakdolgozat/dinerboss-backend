package com.gergo.sb.dinnerboss.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication(exclude = JmxAutoConfiguration.class)
@EnableWebSecurity
public class DinnerBossApplication {
    public static void main(String[] args) {
        SpringApplication.run(DinnerBossApplication.class);
    }
}
