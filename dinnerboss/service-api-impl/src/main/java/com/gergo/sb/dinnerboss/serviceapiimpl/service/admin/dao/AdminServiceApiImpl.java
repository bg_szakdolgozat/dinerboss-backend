package com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.dao;

import com.gergo.sb.dinnerboss.serviceapi.log.Log;
import com.gergo.sb.dinnerboss.serviceapi.rc.exception.UserExistException;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.*;


public class AdminServiceApiImpl implements AdminServiceApi {

    private AdminDao adminDao;

    public AdminServiceApiImpl(AdminDao adminDao) {
        this.adminDao = adminDao;
    }

    @Override
    @Log
    public CreateRemoteAdminResponse createAdmin(CreateRequest request) {
        var account = adminDao.get(request.getAdmin().getEmail());
        if (account != null) {
            throw new UserExistException();
        }
        var admin = request.getAdmin();
        adminDao.create(admin);
        return new CreateRemoteAdminResponse(0);
    }

    @Override
    @Log
    public GetRemoteAdminResponse getAdmin(String email) {
        var response = adminDao.get(email);
        if (response == null) {
            throw new UserExistException();
        }
        return new GetRemoteAdminResponse(0,response);
    }


}
