package com.gergo.sb.dinnerboss.serviceapiimpl.service.report;

import com.gergo.sb.dinnerboss.serviceapi.service.checkout.CheckoutDO;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.report.CreateReportDO;
import com.gergo.sb.dinnerboss.serviceapi.service.report.ReportDO;
import com.gergo.sb.dinnerboss.serviceapi.service.report.ReportSearchParams;

public interface ReportDao {
    void create(CreateReportDO report);

    PagedData<ReportDO> page(PagedRequest page, ReportSearchParams params);
}
