package com.gergo.sb.dinnerboss.serviceapiimpl.service.checkout;

import com.gergo.sb.dinnerboss.serviceapi.service.checkout.*;
import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommodityDO;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;

public interface CheckoutDao {
    void create(CreateCheckoutDO createCheckoutDO);

    PagedData<CheckoutDO> page(PagedRequest page, CheckoutSearchParams params);

    Long getStoreId(Long id);

    void delete(Long id);

    void modify(ModifyCheckoutDO remoteCheckout);

    void addTransmission(AddTransmissionDO checkout);

    PagedData<TransmissionDO> pageTransmissions(PagedRequest page, TransmissionSearchParams params);
}
