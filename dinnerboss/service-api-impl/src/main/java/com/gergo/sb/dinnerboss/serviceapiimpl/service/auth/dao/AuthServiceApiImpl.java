package com.gergo.sb.dinnerboss.serviceapiimpl.service.auth.dao;

import com.gergo.sb.dinnerboss.serviceapi.log.Log;
import com.gergo.sb.dinnerboss.serviceapi.rc.exception.UserExistException;
import com.gergo.sb.dinnerboss.serviceapi.service.auth.AuthServiceApi;
import com.gergo.sb.dinnerboss.serviceapi.service.auth.LoginRemoteAdminRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.auth.LoginRemoteAdminResponse;

public class AuthServiceApiImpl implements AuthServiceApi {

    private AuthDao authDao;

    public AuthServiceApiImpl(AuthDao authDao) {
        this.authDao = authDao;
    }

    @Override
    @Log
    public LoginRemoteAdminResponse loginSuperadmin(String email,String password) {
        var adminProfile= authDao.checkPassword(email,password);
        if(adminProfile==null){
            throw new UserExistException();
        }
        return new LoginRemoteAdminResponse(0,adminProfile);
    }


}
