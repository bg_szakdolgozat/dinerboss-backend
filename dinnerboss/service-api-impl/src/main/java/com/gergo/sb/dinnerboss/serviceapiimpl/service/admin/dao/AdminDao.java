package com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.dao;


import com.gergo.sb.dinnerboss.serviceapi.service.admin.Admin;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.CreateAdminDO;

public interface AdminDao {

    void create(CreateAdminDO createAdminDO);

    Admin get(String email);
}
