package com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.dao;

import com.gergo.sb.dinnerboss.serviceapi.service.admin.AdminSessionDO;

import java.util.Optional;

public interface AdminSessionDao {
    Optional<AdminSessionDO> getAdminSession(String sessionId);
    void generateSession(AdminSessionDO adminSessionDO);

}
