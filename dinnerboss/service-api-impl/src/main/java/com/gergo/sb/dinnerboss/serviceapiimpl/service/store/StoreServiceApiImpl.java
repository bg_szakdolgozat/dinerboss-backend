package com.gergo.sb.dinnerboss.serviceapiimpl.service.store;

import com.gergo.sb.dinnerboss.serviceapi.rc.exception.UserExistException;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.company.GetOwnersByIdResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.store.*;

import java.util.List;

public class StoreServiceApiImpl implements StoreServiceApi {

    private StoreDao storeDao;

    public StoreServiceApiImpl(StoreDao storeDao) {
        this.storeDao = storeDao;
    }

    @Override
    public RemoteBaseResponse createStore(CreateRemoteStoreRequest store) {
        var getStore = store.getStore();
        storeDao.create(getStore);
        var createdStore = storeDao.getStoreByName(store.getStore().getName());
        if (store.getStore().getUserIds() != null) {
            storeDao.assignUserIds(createdStore.getId(), store.getStore().getUserIds());
        }
        return new RemoteBaseResponse(0);
    }

    @Override
    public StoreDO getStoreById(Long id) {
        var store = storeDao.getById(id);
        if (store == null) {
            throw new UserExistException();
        }
        return store;
    }

    @Override
    public void deleteStore(Long id) {
        storeDao.delete(id);
    }

    @Override
    public RemoteBaseResponse modify(StoreDO remoteStore) {
        storeDao.modify(remoteStore);
        return new RemoteBaseResponse(0);
    }

    @Override
    public PageRemoteStoreResponse page(PagedRequest page, StoreSearchParams params) {
        var data = storeDao.page(page, params);
        return new PageRemoteStoreResponse(0, data.getData(), data.getTotalCount());
    }

    @Override
    public GetRemoteStoresByCompIdResponse getStoresByCompId(Long id) {
        var data = storeDao.getStoresByCompId(id);
        if (data == null) {
            throw new UserExistException();
        }
        return new GetRemoteStoresByCompIdResponse(0, data);
    }

    @Override
    public RemoteBaseResponse assignUsers(Long storeId, List<Long> userIds) {
        var isValid = storeDao.getById(storeId);
        if(isValid!=null){
            storeDao.assignUserIds(storeId, userIds);
            return  new RemoteBaseResponse(0);
        }
        return new RemoteBaseResponse(1);
    }

    @Override
    public RemoteBaseResponse revokeUsers(Long storeId, List<Long> userIds) {
        var isValid = storeDao.getById(storeId);
        if(isValid!=null){
            storeDao.revokeUserIds(storeId, userIds);
            return  new RemoteBaseResponse(0);
        }
        return new RemoteBaseResponse(1);
    }

    @Override
    public GetCustomersByIdResponse getByStoreId(Long storeId) {
        var customerIds= storeDao.getCustomerIds(storeId);
        var customers = storeDao.getCustomers(customerIds);
        return new GetCustomersByIdResponse(0,customers);
    }

}
