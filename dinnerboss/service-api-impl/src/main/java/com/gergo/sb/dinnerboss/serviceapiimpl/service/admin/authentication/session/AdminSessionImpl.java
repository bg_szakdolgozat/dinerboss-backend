package com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.authentication.session;

import com.gergo.sb.dinnerboss.serviceapi.service.admin.AdminSessionDO;

import java.time.LocalDateTime;

class AdminSessionImpl implements AdminSession {
    private AdminSessionDO adminSessionDO;

    AdminSessionImpl(AdminSessionDO adminSessionDO) {
        this.adminSessionDO = adminSessionDO;
    }

    @Override
    public String getAdminEmail() {
        return adminSessionDO.getAdminEmail();
    }

    @Override
    public String getSessionId() {
        return adminSessionDO.getSessionId();
    }

    @Override
    public LocalDateTime getExpiry() {
        return adminSessionDO.getExpiry();
    }
}
