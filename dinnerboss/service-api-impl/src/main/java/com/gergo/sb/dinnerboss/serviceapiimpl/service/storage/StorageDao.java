package com.gergo.sb.dinnerboss.serviceapiimpl.service.storage;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.storage.*;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreDO;

import java.util.List;

public interface StorageDao {
    void create(Long storeId);

    void delete(Long id);

    void registerCommodity(RegisterDO request);

    PagedData<RemoteCommodityRegDO> page(PagedRequest page, PageRemoteCommRegistrationsSearch params);

    void registerTool(RegisterDO request);

    PagedData<RemoteCommodityRegDO> pageToolReg(PagedRequest page, PageRemoteCommRegistrationsSearch params);

    List<StorageContentDO> listCommodityByStorage(Long storageId);

    List<StorageContentDO> listToolByStorage(Long storageId);

    List<StorageDO> getAllByStore(List<Long> storeIds);

    List<StorageContentDO> getConsumableCommodities(Long storageId, Long limit);

    StorageDO getById(Long id);
}
