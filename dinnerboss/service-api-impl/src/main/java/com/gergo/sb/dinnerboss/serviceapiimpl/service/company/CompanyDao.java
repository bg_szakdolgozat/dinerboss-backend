package com.gergo.sb.dinnerboss.serviceapiimpl.service.company;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.company.CompanyDO;
import com.gergo.sb.dinnerboss.serviceapi.service.company.CompanySearchParams;
import com.gergo.sb.dinnerboss.serviceapi.service.company.CreateCompanyDO;
import com.gergo.sb.dinnerboss.serviceapi.service.company.StoreIncomeDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;

import java.time.LocalDateTime;
import java.util.List;

public interface CompanyDao {
    void create(CreateCompanyDO company);

    CompanyDO getByName(String name);

    void modify(CompanyDO customer);

    PagedData<CompanyDO> page(PagedRequest page, CompanySearchParams params);

    void addCustomersToCompany(Long id, List<Long> ownerIds);

    List<Long> getOwnerIds(Long id);

    List<CustomerProfileDO> getOwners(List<Long> ownerIds);

    void removeOwner(Long companyId, Long customerId);

    void delete(Long id);

    List<StoreIncomeDO> getStoreIncomes(List<Long> storeIds, Long days);

    List<StoreIncomeDO> getStoreReports(Long storeId, Long days);
}
