package com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.authentication.loginaction;

import com.gergo.sb.dinnerboss.serviceapi.service.admin.Admin;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.authentication.session.AdminSession;

public interface CreateAdminSession {
    AdminSession createAdminSession(Admin admin);
}
