package com.gergo.sb.dinnerboss.serviceapiimpl.service.customer;

import com.gergo.sb.dinnerboss.serviceapi.log.Log;
import com.gergo.sb.dinnerboss.serviceapi.rc.exception.UserExistException;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.*;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.customer.dao.CustomerDao;

public class CustomerServiceApiImpl implements CustomerServiceApi {

    private CustomerDao customerDao;

    public CustomerServiceApiImpl(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @Override
    @Log
    public CreateRemoteCustomerResponse createCustomer(CreateRemoteCustomerRequest admin) {
        var checkAdmin = customerDao.getByEmail(admin.getCustomer().getEmail());
        if (checkAdmin != null) {
            throw new UserExistException();
        }
        var customer = admin.getCustomer();
        customerDao.create(customer);
        var createCustomer = customerDao.getByEmail(admin.getCustomer().getEmail());
        if(admin.getCustomer().getCompIds()!=null){
            customerDao.addCompaniesToCustomer(createCustomer.getId(),customer.getCompIds());
        }
        return new CreateRemoteCustomerResponse(0);
    }

    @Override
    @Log
    public GetRemoteCustomerByEmailResponse getCustomerByEmail(String email) {
        var response = customerDao.getByEmail(email);
        var compIds= customerDao.getCompIds(response.getId());
        if(!compIds.isEmpty()){
            var companies = customerDao.getCompanies(compIds);
            response.setCompanies(companies);
        }
        if (response == null) {
            throw new UserExistException();
        }
        return new GetRemoteCustomerByEmailResponse(0, response);
    }

    @Override
    public PageRemoteCustomerResponse page(PagedRequest page, CustomerSearchParams params) {
        var data = customerDao.page(page, params);
        return new PageRemoteCustomerResponse(0, data.getData(), data.getTotalCount());
    }

    @Override
    public RemoteBaseResponse modify(ModifyRemoteCustomerRequest admin) {
        var checkAdmin = customerDao.getByEmail(admin.getCustomer().getEmail());
        if (checkAdmin != null && !checkAdmin.getEmail().equals(admin.getCustomer().getEmail())) {
            throw new UserExistException();
        }
        var customer = admin.getCustomer();
        customerDao.modify(customer);
        return new RemoteBaseResponse(0);
    }

    @Override
    public void deleteCustomer(Long id) {
        customerDao.delete(id);
    }

    @Override
    public LoginCustomerResponseDO loginAdmin(String email, String password) {
        var customer = customerDao.loginAdmin(email,password);
        if(customer==null){
            throw new UserExistException();
        }
        var compIds= customerDao.getCompIds(customer.getId());

        if(compIds.size()!=0){
            var companies = customerDao.getCompanies(compIds);
            customer.setCompanies(companies);
        }
        var storeIds = customerDao.getStoreIds(customer.getId());
        if(storeIds.size() != 0){
            var stores = customerDao.getStores(storeIds);
            customer.setStores(stores);
        }
        return new LoginCustomerResponseDO(0,customer);
    }

}
