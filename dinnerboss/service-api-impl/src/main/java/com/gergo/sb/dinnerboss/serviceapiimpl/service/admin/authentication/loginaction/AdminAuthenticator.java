package com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.authentication.loginaction;


import com.gergo.sb.dinnerboss.serviceapi.service.admin.Admin;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.AdminDetails;
import com.gergo.sb.dinnerboss.serviceapi.service.admin.CreateAdminDO;

import java.util.Optional;

public interface AdminAuthenticator {
    Optional<AdminDetails> authenticate(String adminId, String password);

}
