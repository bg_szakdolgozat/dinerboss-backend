package com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.authentication.session;


import com.gergo.sb.dinnerboss.serviceapi.service.admin.AdminSessionDO;

import java.math.BigInteger;
import java.time.LocalDateTime;

public interface AdminSession {
    String getAdminEmail();

    String getSessionId();

    LocalDateTime getExpiry();

    static AdminSession getInstance(AdminSessionDO adminSession) {
        return new AdminSessionImpl(adminSession);
    }
}