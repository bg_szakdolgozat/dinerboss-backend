package com.gergo.sb.dinnerboss.serviceapiimpl.service.customer.dao;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.company.CompanyDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CreateCustomerDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerSearchParams;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreDO;

import java.math.BigInteger;
import java.util.List;

public interface CustomerDao {
    void create(CreateCustomerDO customer);

    CustomerProfileDO getByEmail(String email);

    PagedData<CustomerProfileDO> page(PagedRequest page, CustomerSearchParams params);

    void modify(CustomerProfileDO customer);

    void addCompaniesToCustomer(Long id, List<Long> compIds);


    List<Long> getCompIds(Long id);

    List<Long> getStoreIds(Long id);

    List<CompanyDO> getCompanies(List<Long> compIds);

    List<StoreDO> getStores(List<Long> storeIds);


    void delete(Long id);

    CustomerProfileDO loginAdmin(String email, String password);

    CustomerProfileDO getById(Long id);

    void setCheckoutMoney(Long id, Boolean in, Long money);
}
