package com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.authentication.loginaction;



class AdminAuthenticationTokenImpl implements AdminAuthenticationToken {
    private String email;
    private String password;

	public AdminAuthenticationTokenImpl(String email, String password) {
		this.email = email;
		this.password = password;
	}




    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }
}
