package com.gergo.sb.dinnerboss.serviceapiimpl.service.commodity;

import com.gergo.sb.dinnerboss.serviceapi.service.commodity.*;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;

public class CommodityServiceApiImpl implements CommodityServiceApi {

    private CommodityDao commodityDao;

    public CommodityServiceApiImpl(CommodityDao commodityDao) {
        this.commodityDao = commodityDao;
    }

    @Override
    public RemoteBaseResponse createCommodity(CreateRemoteCommodityRequest commodity) {
        commodityDao.create(commodity.getCommodity());
        return new RemoteBaseResponse(0);
    }

    @Override
    public PageRemoteCommodityResponse page(PagedRequest page, CommoditySearchParams params) {
        var data = commodityDao.page(page, params);
        return new PageRemoteCommodityResponse(0, data.getData(), data.getTotalCount());
    }

    @Override
    public RemoteBaseResponse modify(CommodityDO remoteCommodity) {
        commodityDao.modify(remoteCommodity);
        return new RemoteBaseResponse(0);
    }

    @Override
    public void deleteStore(Long id) {
        commodityDao.delete(id);
    }

    @Override
    public RemoteBaseResponse createTool(CreateRemoteCommodityRequest tool) {
        commodityDao.createTool(tool.getCommodity());
        return new RemoteBaseResponse(0);
    }

    @Override
    public PageRemoteCommodityResponse pageTools(PagedRequest page, CommoditySearchParams params) {
        var data = commodityDao.pageTools(page, params);
        return new PageRemoteCommodityResponse(0, data.getData(), data.getTotalCount());
    }

    @Override
    public void deleteTool(Long id) {
        commodityDao.deleteTool(id);

    }
}
