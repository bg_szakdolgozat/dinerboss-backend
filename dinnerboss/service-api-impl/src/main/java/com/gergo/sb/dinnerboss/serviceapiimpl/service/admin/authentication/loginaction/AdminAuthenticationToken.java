package com.gergo.sb.dinnerboss.serviceapiimpl.service.admin.authentication.loginaction;

public interface AdminAuthenticationToken {
    String getEmail();
    String  getPassword();

	static AdminAuthenticationToken getInstance(String email, String password) {
		return new AdminAuthenticationTokenImpl(email, password);
	}
}
