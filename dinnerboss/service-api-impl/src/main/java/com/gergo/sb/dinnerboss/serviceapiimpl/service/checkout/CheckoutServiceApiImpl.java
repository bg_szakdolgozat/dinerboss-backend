package com.gergo.sb.dinnerboss.serviceapiimpl.service.checkout;

import com.gergo.sb.dinnerboss.serviceapi.service.checkout.*;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.customer.dao.CustomerDao;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.store.StoreDao;

public class CheckoutServiceApiImpl implements CheckoutServiceApi {

    private CheckoutDao checkoutDao;
    private StoreDao storeDao;
    private CustomerDao customerDao;

    public CheckoutServiceApiImpl(CheckoutDao checkoutDao, StoreDao storeDao,CustomerDao customerDao) {
        this.checkoutDao = checkoutDao;
        this.storeDao = storeDao;
        this.customerDao = customerDao;
    }

    @Override
    public RemoteBaseResponse createCommodity(CreateRemoteCheckoutRequest checkout) {
        checkoutDao.create(checkout.getCreateCheckoutDO());
        return new RemoteBaseResponse(0);
    }

    @Override
    public PageRemoteCheckoutResponse page(PagedRequest page, CheckoutSearchParams params) {
        var data = checkoutDao.page(page, params);
        data.getData().forEach((a) -> {
            var storeId = checkoutDao.getStoreId(a.getId());
            var store = storeDao.getById(storeId);
            a.setStoreDO(store);
        });
        return new PageRemoteCheckoutResponse(0, data.getData(), data.getTotalCount());
    }

    @Override
    public void deleteCheckout(Long id) {
        checkoutDao.delete(id);
    }

    @Override
    public RemoteBaseResponse modify(ModifyCheckoutDO remoteCheckout) {
         checkoutDao.modify(remoteCheckout);
        return new RemoteBaseResponse(0);
    }

    @Override
    public RemoteBaseResponse addTransmission(AddTransmissionDO checkout) {
        checkoutDao.addTransmission(checkout);
        customerDao.setCheckoutMoney(checkout.getCheckoutId(),checkout.getIn(),checkout.getMoney());
        return new RemoteBaseResponse(0);
    }

    @Override
    public PageRemoteTransmissionResponse pageTransmissions(PagedRequest page, TransmissionSearchParams params) {
        var data = checkoutDao.pageTransmissions(page, params);
        data.getData().forEach((a) -> {
            var customer = customerDao.getById(a.getCustomer().getId());
            a.setCustomer(customer);
        });
        return new PageRemoteTransmissionResponse(0, data.getData(), data.getTotalCount());
    }
}
