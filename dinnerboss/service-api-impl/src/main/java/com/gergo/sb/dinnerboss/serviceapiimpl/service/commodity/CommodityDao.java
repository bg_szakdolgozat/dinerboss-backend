package com.gergo.sb.dinnerboss.serviceapiimpl.service.commodity;

import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommodityDO;
import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommoditySearchParams;
import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CreateCommodityDO;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;

public interface CommodityDao {
    void create(CreateCommodityDO commodity);

    PagedData<CommodityDO> page(PagedRequest page, CommoditySearchParams params);

    void modify(CommodityDO remoteCommodity);

    void delete(Long id);

    void createTool(CreateCommodityDO tool);

    PagedData<CommodityDO> pageTools(PagedRequest page, CommoditySearchParams params);

    void deleteTool(Long id);
}
