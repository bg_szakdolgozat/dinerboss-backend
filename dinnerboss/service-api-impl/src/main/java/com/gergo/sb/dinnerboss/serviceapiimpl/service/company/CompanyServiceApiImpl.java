package com.gergo.sb.dinnerboss.serviceapiimpl.service.company;

import com.gergo.sb.dinnerboss.serviceapi.rc.exception.UserExistException;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.company.*;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.PageRemoteCustomerResponse;

import java.time.LocalDateTime;
import java.util.List;

public class CompanyServiceApiImpl implements CompanyServiceApi {
    private CompanyDao companyDao;

    public CompanyServiceApiImpl(CompanyDao companyDao) {
        this.companyDao = companyDao;
    }

    @Override
    public RemoteBaseResponse createCompany(CreateRemoteCompanyRequest admin) {
//        var checkAdmin = companyDao.getByEmail(admin.getCustomer().getEmail());
//        if(checkAdmin!=null){
//            throw new UserExistException();
//        }

        var company = admin.getCompany();
        companyDao.create(company);
        var createCompany = companyDao.getByName(admin.getCompany().getName());

        if(admin.getCompany().getOwnerIds()!=null){
            companyDao.addCustomersToCompany(createCompany.getId(),admin.getCompany().getOwnerIds());
        }
        return new RemoteBaseResponse(0);
    }

    @Override
    public GetRemoteCompanyByName getCompanyByName(String name) {
        var response = companyDao.getByName(name);
        var ownerIds= companyDao.getOwnerIds(response.getId());
        var owners = companyDao.getOwners(ownerIds);
        response.setOwners(owners);
        if (response == null) {
            throw new UserExistException();
        }
        return new GetRemoteCompanyByName(0, response);
    }

    @Override
    public RemoteBaseResponse modify(ModifyRemoteCompanyRequest company) {
        var checkAdmin = companyDao.getByName(company.getCompany().getName());
        if (checkAdmin != null && !checkAdmin.getName().equals(company.getCompany().getName())) {
            throw new UserExistException();
        }
        var customer = company.getCompany();
        companyDao.modify(customer);
        return new RemoteBaseResponse(0);
    }

    @Override
    public PageRemoteCompaniesResponse page(PagedRequest page, CompanySearchParams params) {
        var data = companyDao.page(page, params);
        return new PageRemoteCompaniesResponse(0, data.getData(), data.getTotalCount());
    }

    @Override
    public RemoteBaseResponse removeOwner(Long companyId, Long customerId) {
        companyDao.removeOwner(companyId,customerId);
        return new RemoteBaseResponse(0);
    }

    @Override
    public void deleteCompany(Long id) {
        companyDao.delete(id);
    }

    @Override
    public RemoteBaseResponse assignOwners(Long companyId, List<Long> customerIds) {
        companyDao.addCustomersToCompany(companyId,customerIds);
        return new RemoteBaseResponse(0);
    }

    @Override
    public GetOwnersByIdResponse getByCompanyId(Long compId) {
        var ownerIds= companyDao.getOwnerIds(compId);
        var owners = companyDao.getOwners(ownerIds);
        return new GetOwnersByIdResponse(0,owners);
    }

    @Override
    public StoreIncomeResponse getStoreIncomes(List<Long> storeIds, Long days) {
        var resp = companyDao.getStoreIncomes(storeIds, days);
       return new StoreIncomeResponse(0,resp);

    }

    @Override
    public StoreIncomeResponse getStoreReports(Long storeId, Long days) {
        var resp = companyDao.getStoreReports(storeId, days);
        return new StoreIncomeResponse(0,resp);
    }

}
