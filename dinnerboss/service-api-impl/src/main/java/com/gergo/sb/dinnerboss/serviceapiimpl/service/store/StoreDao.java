package com.gergo.sb.dinnerboss.serviceapiimpl.service.store;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedData;
import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.store.CreateStoreDO;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreDO;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreSearchParams;

import java.util.List;

public interface StoreDao {
    void create(CreateStoreDO company);

    StoreDO getById(Long id);

    void delete(Long id);

    void modify(StoreDO remoteStore);

    PagedData<StoreDO> page(PagedRequest page, StoreSearchParams params);

    List<StoreDO> getStoresByCompId(Long id);

    StoreDO getStoreByName(String name);

    void assignUserIds(Long id, List<Long> userIds);

    void revokeUserIds(Long storeId, List<Long> userIds);

    List<Long> getCustomerIds(Long storeId);

    List<CustomerProfileDO> getCustomers(List<Long> customerIds);
}
