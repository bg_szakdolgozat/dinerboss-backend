package com.gergo.sb.dinnerboss.serviceapiimpl.service.report;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.report.CreateRemoteReportRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.report.PageRemoteReportResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.report.ReportSearchParams;
import com.gergo.sb.dinnerboss.serviceapi.service.report.ReportServiceApi;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.customer.dao.CustomerDao;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.store.StoreDao;

public class ReportServiceApiImpl implements ReportServiceApi {
    public ReportDao reportDao;
    public StoreDao storeDao;
    public CustomerDao customerDao;

    public ReportServiceApiImpl(ReportDao reportDao, StoreDao storeDao, CustomerDao customerDao) {
        this.reportDao = reportDao;
        this.storeDao = storeDao;
        this.customerDao = customerDao;
    }

    @Override
    public RemoteBaseResponse createReport(CreateRemoteReportRequest report) {
        reportDao.create(report.getReport());
        return new RemoteBaseResponse(0);
    }

    @Override
    public PageRemoteReportResponse page(PagedRequest page, ReportSearchParams params) {
        var data = reportDao.page(page, params);
        data.getData().forEach((a) -> {
            var store = storeDao.getById(a.getStore().getId());
            a.setStore(store);
            var customer = customerDao.getById(a.getCustomer().getId());
            a.setCustomer(customer);
        });
        return new PageRemoteReportResponse(0,data.getData(),data.getTotalCount());
    }
}
