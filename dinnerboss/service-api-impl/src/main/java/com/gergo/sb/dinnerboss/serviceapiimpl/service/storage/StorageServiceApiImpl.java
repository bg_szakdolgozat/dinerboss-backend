package com.gergo.sb.dinnerboss.serviceapiimpl.service.storage;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.storage.*;
import com.gergo.sb.dinnerboss.serviceapiimpl.service.store.StoreDao;

import java.util.List;

public class StorageServiceApiImpl implements StorageServiceApi {

    private StorageDao storageDao;
    private StoreDao storeDao;

    public StorageServiceApiImpl(StorageDao storageDao, StoreDao storeDao) {
        this.storageDao = storageDao;
        this.storeDao = storeDao;
    }

    @Override
    public RemoteBaseResponse createStorage(Long storeId) {
        storageDao.create(storeId);
        return new RemoteBaseResponse(0);
    }

    @Override
    public void deleteStorage(Long id) {
        storageDao.delete(id);
    }

    @Override
    public RemoteBaseResponse registerCommodity(RegisterDO request) {
        storageDao.registerCommodity(request);
        return new RemoteBaseResponse(0);
    }

    @Override
    public PageRemoteCommRegistrationResponse page(PagedRequest page, PageRemoteCommRegistrationsSearch params) {
        var data = storageDao.page(page, params);
        return new PageRemoteCommRegistrationResponse(0, data.getData(), data.getTotalCount());
    }

    @Override
    public RemoteBaseResponse registerTool(RegisterDO request) {
        storageDao.registerTool(request);
        return new RemoteBaseResponse(0);
    }

    @Override
    public PageRemoteCommRegistrationResponse pageToolReg(PagedRequest page, PageRemoteCommRegistrationsSearch params) {
        var data = storageDao.pageToolReg(page, params);
        return new PageRemoteCommRegistrationResponse(0, data.getData(), data.getTotalCount());
    }

    @Override
    public ListCommodityByStorageRemoteResponse listCommodityByStorage(Long storageId) {

        var responseData = storageDao.listCommodityByStorage(storageId);
        return new ListCommodityByStorageRemoteResponse(0,responseData);
    }

    @Override
    public ListCommodityByStorageRemoteResponse listToolByStorage(Long storageId) {
        var responseData = storageDao.listToolByStorage(storageId);
        return new ListCommodityByStorageRemoteResponse(0,responseData);
    }

    @Override
    public GetAllByStoreRemoteResponse getAllByStore(List<Long> storeId) {
        var responseData = storageDao.getAllByStore(storeId);
        responseData.forEach((a)->{
            a.setStore(storeDao.getById(a.getStore().getId()));
        });
        return new GetAllByStoreRemoteResponse(0,responseData);
    }

    @Override
    public ListCommodityByStorageRemoteResponse getConsumableCommodities(Long storageId,Long limit) {
        var responseData = storageDao.getConsumableCommodities(storageId,limit);
        return new ListCommodityByStorageRemoteResponse(0,responseData);
    }

    @Override
    public StorageDO getById(Long id) {
        var resp = storageDao.getById(id);
        resp.setStore(storeDao.getById(resp.getStore().getId()));
        return resp;
    }
}
