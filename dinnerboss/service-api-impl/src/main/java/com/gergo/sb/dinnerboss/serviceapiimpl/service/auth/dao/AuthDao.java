package com.gergo.sb.dinnerboss.serviceapiimpl.service.auth.dao;

import com.gergo.sb.dinnerboss.serviceapi.service.auth.RemoteAdminProfile;

public interface AuthDao {
    RemoteAdminProfile checkPassword(String adminEmail,String password);


}
