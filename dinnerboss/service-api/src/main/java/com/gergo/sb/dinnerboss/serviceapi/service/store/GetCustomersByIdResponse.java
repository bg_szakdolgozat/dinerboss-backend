package com.gergo.sb.dinnerboss.serviceapi.service.store;

import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;

import java.util.List;

public class GetCustomersByIdResponse {
    private Integer rc;

    private List<CustomerProfileDO> customers = null;

    public GetCustomersByIdResponse(Integer rc, List<CustomerProfileDO> customers) {
        this.rc = rc;
        this.customers = customers;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public List<CustomerProfileDO> getCustomers() {
        return customers;
    }

    public void setCustomers(List<CustomerProfileDO> customers) {
        this.customers = customers;
    }
}
