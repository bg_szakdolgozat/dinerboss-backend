package com.gergo.sb.dinnerboss.serviceapi.service.checkout;

import java.util.ArrayList;
import java.util.List;

public class PageRemoteTransmissionResponse {
    private Integer rc;

    private List<TransmissionDO> data = new ArrayList<>();

    private Long totalCount;

    public PageRemoteTransmissionResponse() {
    }

    public PageRemoteTransmissionResponse(Integer rc, List<TransmissionDO> data, Long totalCount) {
        this.rc = rc;
        this.data = data;
        this.totalCount = totalCount;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public List<TransmissionDO> getData() {
        return data;
    }

    public void setData(List<TransmissionDO> data) {
        this.data = data;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
