package com.gergo.sb.dinnerboss.serviceapi.service.store;

import java.time.LocalDateTime;
import java.util.List;

public class CreateStoreDO {

    private String name;

    private LocalDateTime opened;

    private String location;

    private String phoneNumber;

    private Long compId;

    private List<Long> userIds ;


    public CreateStoreDO() {
    }

    public CreateStoreDO(String name, LocalDateTime opened, String location, String phoneNumber, Long compId, List<Long> userIds) {
        this.name = name;
        this.opened = opened;
        this.location = location;
        this.phoneNumber = phoneNumber;
        this.compId = compId;
        this.userIds = userIds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getOpened() {
        return opened;
    }

    public void setOpened(LocalDateTime opened) {
        this.opened = opened;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getCompId() {
        return compId;
    }

    public void setCompId(Long compId) {
        this.compId = compId;
    }

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }
}
