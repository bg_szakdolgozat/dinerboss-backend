package com.gergo.sb.dinnerboss.serviceapi.service.auth;

public class LoginRemoteAdminResponse {
    private Integer rc;
    private RemoteAdminProfile admin;

    public LoginRemoteAdminResponse(Integer rc, RemoteAdminProfile admin) {
        this.rc = rc;
        this.admin = admin;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public RemoteAdminProfile getAdmin() {
        return admin;
    }

    public void setAdmin(RemoteAdminProfile admin) {
        this.admin = admin;
    }
}
