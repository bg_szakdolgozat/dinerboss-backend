package com.gergo.sb.dinnerboss.serviceapi.service.customer;

import java.util.ArrayList;
import java.util.List;

public class PageRemoteCustomerResponse {
    private Integer rc;
    private List<CustomerProfileDO> data = new ArrayList<>();
    private Long totalCount;

    public PageRemoteCustomerResponse(Integer rc, List<CustomerProfileDO> data, Long totalCount) {
        this.rc = rc;
        this.data = data;
        this.totalCount = totalCount;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public List<CustomerProfileDO> getData() {
        return data;
    }

    public void setData(List<CustomerProfileDO> data) {
        this.data = data;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
