package com.gergo.sb.dinnerboss.serviceapi.service.store;

public class CreateRemoteStoreRequest {

    private CreateStoreDO store;

    public CreateRemoteStoreRequest() {
    }

    public CreateRemoteStoreRequest(CreateStoreDO store) {
        this.store = store;
    }

    public CreateStoreDO getStore() {
        return store;
    }

    public void setStore(CreateStoreDO store) {
        this.store = store;
    }
}
