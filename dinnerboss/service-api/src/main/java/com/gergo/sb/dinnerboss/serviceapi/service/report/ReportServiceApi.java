package com.gergo.sb.dinnerboss.serviceapi.service.report;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;

public interface ReportServiceApi {

    RemoteBaseResponse createReport(CreateRemoteReportRequest report);

    PageRemoteReportResponse page(PagedRequest map, ReportSearchParams map1);
}
