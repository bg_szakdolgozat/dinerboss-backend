package com.gergo.sb.dinnerboss.serviceapi.service.company;

import java.util.ArrayList;
import java.util.List;

public class PageRemoteCompaniesResponse {

    private Integer rc;
    private List<CompanyDO> data = new ArrayList<>();
    private Long totalCount;

    public PageRemoteCompaniesResponse(Integer rc, List<CompanyDO> data, Long totalCount) {
        this.rc = rc;
        this.data = data;
        this.totalCount = totalCount;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public List<CompanyDO> getData() {
        return data;
    }

    public void setData(List<CompanyDO> data) {
        this.data = data;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
