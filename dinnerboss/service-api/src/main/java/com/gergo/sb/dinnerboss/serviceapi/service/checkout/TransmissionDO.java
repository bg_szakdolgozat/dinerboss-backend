package com.gergo.sb.dinnerboss.serviceapi.service.checkout;

import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;

import java.time.LocalDateTime;

public class TransmissionDO {
    private Long id;

    private LocalDateTime date;

    private String comment;

    private Boolean invalue;

    private CustomerProfileDO customer;

    private CheckoutDO checkout;

    private Long money;

    public TransmissionDO() {
    }

    public TransmissionDO(Long id, LocalDateTime date, String comment, Boolean invalue, CustomerProfileDO customer, CheckoutDO checkout, Long money) {
        this.id = id;
        this.date = date;
        this.comment = comment;
        this.invalue = invalue;
        this.customer = customer;
        this.checkout = checkout;
        this.money = money;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getInvalue() {
        return invalue;
    }

    public void setInvalue(Boolean invalue) {
        this.invalue = invalue;
    }

    public CustomerProfileDO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerProfileDO customer) {
        this.customer = customer;
    }

    public CheckoutDO getCheckout() {
        return checkout;
    }

    public void setCheckout(CheckoutDO checkout) {
        this.checkout = checkout;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }
}
