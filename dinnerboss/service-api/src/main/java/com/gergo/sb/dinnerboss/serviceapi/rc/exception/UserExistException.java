package com.gergo.sb.dinnerboss.serviceapi.rc.exception;


import com.gergo.sb.dinnerboss.serviceapi.rc.ReturnCode;

public class UserExistException extends ReturnCodeAwareException {

    public UserExistException() {
        super("User already exist!");
    }

    @Override
    public int getRc() {
        return ReturnCode.USER_ALREADY_EXIST.getCode();
    }
}
