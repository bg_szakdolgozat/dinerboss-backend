package com.gergo.sb.dinnerboss.serviceapi.service.customer;


import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;

public interface CustomerServiceApi {
    CreateRemoteCustomerResponse createCustomer(CreateRemoteCustomerRequest admin);

    GetRemoteCustomerByEmailResponse getCustomerByEmail(String email);

    PageRemoteCustomerResponse page(PagedRequest page, CustomerSearchParams params);

    RemoteBaseResponse modify(ModifyRemoteCustomerRequest admin);

    void deleteCustomer(Long id);

    LoginCustomerResponseDO loginAdmin(String email, String password);
}
