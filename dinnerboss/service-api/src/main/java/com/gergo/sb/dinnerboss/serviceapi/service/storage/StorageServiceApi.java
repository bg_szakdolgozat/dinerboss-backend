package com.gergo.sb.dinnerboss.serviceapi.service.storage;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;

import java.util.List;

public interface StorageServiceApi {
    RemoteBaseResponse createStorage(Long storeId);

    void deleteStorage(Long id);

    RemoteBaseResponse registerCommodity(RegisterDO map);

    PageRemoteCommRegistrationResponse page(PagedRequest page, PageRemoteCommRegistrationsSearch params);

    RemoteBaseResponse registerTool(RegisterDO map);

    PageRemoteCommRegistrationResponse pageToolReg(PagedRequest map, PageRemoteCommRegistrationsSearch map1);

    ListCommodityByStorageRemoteResponse listCommodityByStorage(Long storageId);

    ListCommodityByStorageRemoteResponse listToolByStorage(Long storageId);

    GetAllByStoreRemoteResponse getAllByStore(List<Long> storeId);

    ListCommodityByStorageRemoteResponse getConsumableCommodities(Long storageId,Long limit);

    StorageDO getById(Long id);
}
