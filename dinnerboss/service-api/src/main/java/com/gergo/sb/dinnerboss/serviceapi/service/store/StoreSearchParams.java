package com.gergo.sb.dinnerboss.serviceapi.service.store;

import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteSearchParams;

import java.util.List;

public class StoreSearchParams extends RemoteSearchParams {
    private String value;
    private String name;
    private Long compid;
    private List<Long> compIds = null;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCompid() {
        return compid;
    }

    public void setCompid(Long compid) {
        this.compid = compid;
    }

    public List<Long> getCompIds() {
        return compIds;
    }

    public void setCompIds(List<Long> compIds) {
        this.compIds = compIds;
    }
}
