package com.gergo.sb.dinnerboss.serviceapi.service.report;

import java.util.List;

public class ReportSearchParams {
    private String value;

    private String date;

    private List<Long> storeIds = null;

    private List<Long> customerIds = null;

    public ReportSearchParams() {
    }

    public ReportSearchParams(String value, String date, List<Long> storeIds, List<Long> customerIds) {
        this.value = value;
        this.date = date;
        this.storeIds = storeIds;
        this.customerIds = customerIds;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Long> getStoreIds() {
        return storeIds;
    }

    public void setStoreIds(List<Long> storeIds) {
        this.storeIds = storeIds;
    }

    public List<Long> getCustomerIds() {
        return customerIds;
    }

    public void setCustomerIds(List<Long> customerIds) {
        this.customerIds = customerIds;
    }
}
