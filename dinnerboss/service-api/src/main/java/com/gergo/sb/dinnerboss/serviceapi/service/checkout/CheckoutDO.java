package com.gergo.sb.dinnerboss.serviceapi.service.checkout;

import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreDO;

public class CheckoutDO {
    private Long id;

    private String name;

    private Long money;

    private String comment;

    private StoreDO store;

    public CheckoutDO() {
    }

    public CheckoutDO(Long id, String name, Long money, String comment, StoreDO store) {
        this.id = id;
        this.name = name;
        this.money = money;
        this.comment = comment;
        this.store = store;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public StoreDO getStore() {
        return store;
    }

    public void setStoreDO(StoreDO store) {
        this.store = store;
    }
}
