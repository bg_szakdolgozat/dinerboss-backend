package com.gergo.sb.dinnerboss.serviceapi.service.company;

import java.util.List;

public class StoreIncomeDO {
    private String name;


    private List<SeriesDO> series = null;

    public StoreIncomeDO() {
    }

    public StoreIncomeDO(String name, List<SeriesDO> series) {
        this.name = name;
        this.series = series;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SeriesDO> getSeries() {
        return series;
    }

    public void setSeries(List<SeriesDO> series) {
        this.series = series;
    }
}
