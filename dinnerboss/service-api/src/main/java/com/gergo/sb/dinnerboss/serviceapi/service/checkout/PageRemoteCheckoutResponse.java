package com.gergo.sb.dinnerboss.serviceapi.service.checkout;

import java.util.ArrayList;
import java.util.List;

public class PageRemoteCheckoutResponse {
    private Integer rc;


    private List<CheckoutDO> data = new ArrayList<>();

    private Long totalCount;

    public PageRemoteCheckoutResponse() {
    }

    public PageRemoteCheckoutResponse(Integer rc, List<CheckoutDO> data, Long totalCount) {
        this.rc = rc;
        this.data = data;
        this.totalCount = totalCount;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public List<CheckoutDO> getData() {
        return data;
    }

    public void setData(List<CheckoutDO> data) {
        this.data = data;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
