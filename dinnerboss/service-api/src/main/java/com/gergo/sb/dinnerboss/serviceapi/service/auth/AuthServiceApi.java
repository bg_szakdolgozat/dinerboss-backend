package com.gergo.sb.dinnerboss.serviceapi.service.auth;

public interface AuthServiceApi {
    LoginRemoteAdminResponse loginSuperadmin(String email, String password);
}
