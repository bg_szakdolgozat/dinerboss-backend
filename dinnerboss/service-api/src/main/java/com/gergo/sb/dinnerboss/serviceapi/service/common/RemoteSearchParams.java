package com.gergo.sb.dinnerboss.serviceapi.service.common;

public class RemoteSearchParams {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
