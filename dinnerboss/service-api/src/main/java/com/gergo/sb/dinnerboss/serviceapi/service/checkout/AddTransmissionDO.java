package com.gergo.sb.dinnerboss.serviceapi.service.checkout;

public class AddTransmissionDO {
    private String comment;

    private Boolean in;

    private Long money;

    private Long customerId;

    private Long checkoutId;

    public AddTransmissionDO() {
    }

    public AddTransmissionDO(String comment, Boolean in, Long money, Long customerId, Long checkoutId) {
        this.comment = comment;
        this.in = in;
        this.money = money;
        this.customerId = customerId;
        this.checkoutId = checkoutId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getIn() {
        return in;
    }

    public void setIn(Boolean in) {
        this.in = in;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getCheckoutId() {
        return checkoutId;
    }

    public void setCheckoutId(Long checkoutId) {
        this.checkoutId = checkoutId;
    }
}
