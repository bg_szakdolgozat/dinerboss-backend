package com.gergo.sb.dinnerboss.serviceapi.service.customer;

import com.gergo.sb.dinnerboss.serviceapi.service.company.CompanyDO;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreDO;

import java.util.List;

public class CustomerProfileDO {
    private Long id;

    private String title;

    private String firstName;

    private String lastName;

    private Long status;

    private String phoneNumber;

    private String email;

    private List<CompanyDO> companies = null;

    private List <StoreDO> stores = null;

    public CustomerProfileDO() {
    }

    public CustomerProfileDO(Long id, String title, String firstName, String lastName, Long status, String phoneNumber, String email, List<CompanyDO> companies, List<StoreDO> stores) {
        this.id = id;
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.companies = companies;
        this.stores = stores;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<CompanyDO> getCompanies() {
        return companies;
    }

    public void setCompanies(List<CompanyDO> companies) {
        this.companies = companies;
    }

    public List<StoreDO> getStores() {
        return stores;
    }

    public void setStores(List<StoreDO> stores) {
        this.stores = stores;
    }
}
