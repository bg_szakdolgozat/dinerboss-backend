package com.gergo.sb.dinnerboss.serviceapi.service.company;

import java.time.LocalDateTime;
import java.util.List;

public class CreateCompanyDO {
    private String name;
    private LocalDateTime founded;
    private String address;
    private List<Long> ownerIds;

    public CreateCompanyDO() {
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getFounded() {
        return founded;
    }

    public void setFounded(LocalDateTime founded) {
        this.founded = founded;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Long> getOwnerIds() {
        return ownerIds;
    }

    public void setOwnerIds(List<Long> ownerIds) {
        this.ownerIds = ownerIds;
    }
}
