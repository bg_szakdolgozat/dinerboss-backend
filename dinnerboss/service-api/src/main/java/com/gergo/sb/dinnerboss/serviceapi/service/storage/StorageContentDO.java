package com.gergo.sb.dinnerboss.serviceapi.service.storage;

import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommodityDO;

public class StorageContentDO {

    private CommodityDO commodity;

    private Long total;

    public StorageContentDO() {
    }

    public CommodityDO getCommodity() {
        return commodity;
    }

    public void setCommodity(CommodityDO commodity) {
        this.commodity = commodity;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
