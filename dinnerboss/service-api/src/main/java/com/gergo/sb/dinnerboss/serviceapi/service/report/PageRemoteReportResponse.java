package com.gergo.sb.dinnerboss.serviceapi.service.report;

import java.util.ArrayList;
import java.util.List;

public class PageRemoteReportResponse {
    private Integer rc;

    private List<ReportDO> data ;
    private Long totalCount;

    public PageRemoteReportResponse(Integer rc, List<ReportDO> data, Long totalCount) {
        this.rc = rc;
        this.data = data;
        this.totalCount = totalCount;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public List<ReportDO> getData() {
        return data;
    }

    public void setData(List<ReportDO> data) {
        this.data = data;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
