package com.gergo.sb.dinnerboss.serviceapi.service.storage;

import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommodityDO;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;

import java.math.BigInteger;
import java.time.LocalDateTime;

public class RemoteCommodityRegDO {

    private BigInteger id;

    private LocalDateTime date;

    private Long quantity;

    private Boolean invalue;

    private CommodityDO commodity;

    private CustomerProfileDO customer;

    private Long storageId;

    private Long customerId;

    private Long comId;

    public RemoteCommodityRegDO() {
    }

    public RemoteCommodityRegDO(BigInteger id, LocalDateTime date, Long quantity, Boolean invalue, CommodityDO commodity, CustomerProfileDO customer, Long storageId, Long customerId, Long comId) {
        this.id = id;
        this.date = date;
        this.quantity = quantity;
        this.invalue = invalue;
        this.commodity = commodity;
        this.customer = customer;
        this.storageId = storageId;
        this.customerId = customerId;
        this.comId = comId;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Boolean getInvalue() {
        return invalue;
    }

    public void setInvalue(Boolean invalue) {
        this.invalue = invalue;
    }

    public CommodityDO getCommodity() {
        return commodity;
    }

    public void setCommodity(CommodityDO commodity) {
        this.commodity = commodity;
    }

    public CustomerProfileDO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerProfileDO customer) {
        this.customer = customer;
    }

    public Long getStorageId() {
        return storageId;
    }

    public void setStorageId(Long storageId) {
        this.storageId = storageId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getComId() {
        return comId;
    }

    public void setComId(Long comId) {
        this.comId = comId;
    }
}
