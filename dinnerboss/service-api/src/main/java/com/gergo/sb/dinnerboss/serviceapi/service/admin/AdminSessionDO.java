package com.gergo.sb.dinnerboss.serviceapi.service.admin;


import java.math.BigInteger;
import java.time.LocalDateTime;

public class AdminSessionDO {
    private BigInteger id;
    private String adminEmail;
    private String sessionId;
    private LocalDateTime expiry;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public LocalDateTime getExpiry() {
        return expiry;
    }

    public void setExpiry(LocalDateTime expiry) {
        this.expiry = expiry;
    }
}
