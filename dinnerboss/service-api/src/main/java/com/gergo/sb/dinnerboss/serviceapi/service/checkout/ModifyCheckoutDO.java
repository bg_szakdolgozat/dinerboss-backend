package com.gergo.sb.dinnerboss.serviceapi.service.checkout;

public class ModifyCheckoutDO {
    private Long id;

    private String name;
    private String comment;
    private Long money;
    private Long storeId;

    public ModifyCheckoutDO() {
    }

    public ModifyCheckoutDO(Long id, String name, String comment, Long money, Long storeId) {
        this.id = id;
        this.name = name;
        this.comment = comment;
        this.money = money;
        this.storeId = storeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }
}
