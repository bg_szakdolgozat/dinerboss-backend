package com.gergo.sb.dinnerboss.serviceapi.service.storage;

public class RegisterDO {

    private Long valueId;

    private Long storageId;

    private Long customerId;

    private Long quantity;

    private Boolean in;

    public RegisterDO() {
    }

    public RegisterDO(Long valueId, Long storageId, Long customerId, Long quantity, Boolean in) {
        this.valueId = valueId;
        this.storageId = storageId;
        this.customerId = customerId;
        this.quantity = quantity;
        this.in = in;
    }

    public Long getValueId() {
        return valueId;
    }

    public void setValueId(Long valueId) {
        this.valueId = valueId;
    }

    public Long getStorageId() {
        return storageId;
    }

    public void setStorageId(Long storageId) {
        this.storageId = storageId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Boolean getIn() {
        return in;
    }

    public void setIn(Boolean in) {
        this.in = in;
    }
}
