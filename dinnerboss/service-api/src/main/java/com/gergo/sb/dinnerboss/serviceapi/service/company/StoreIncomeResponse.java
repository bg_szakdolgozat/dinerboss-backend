package com.gergo.sb.dinnerboss.serviceapi.service.company;

import java.util.List;

public class StoreIncomeResponse {
    private Integer rc;

    private List<StoreIncomeDO> data = null;

    public StoreIncomeResponse() {
    }

    public StoreIncomeResponse(Integer rc, List<StoreIncomeDO> data) {
        this.rc = rc;
        this.data = data;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public List<StoreIncomeDO> getData() {
        return data;
    }

    public void setData(List<StoreIncomeDO> data) {
        this.data = data;
    }
}
