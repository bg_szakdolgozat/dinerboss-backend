package com.gergo.sb.dinnerboss.serviceapi.service.commodity;

import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;

import java.util.ArrayList;
import java.util.List;

public class PageRemoteCommodityResponse {
    private Integer rc;
    private List<CommodityDO> data = new ArrayList<>();
    private Long totalCount;

    public PageRemoteCommodityResponse(Integer rc, List<CommodityDO> data, Long totalCount) {
        this.rc = rc;
        this.data = data;
        this.totalCount = totalCount;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public List<CommodityDO> getData() {
        return data;
    }

    public void setData(List<CommodityDO> data) {
        this.data = data;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
