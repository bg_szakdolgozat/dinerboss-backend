package com.gergo.sb.dinnerboss.serviceapi.service.commodity;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;

public interface CommodityServiceApi {

    RemoteBaseResponse createCommodity(CreateRemoteCommodityRequest commodity);

    PageRemoteCommodityResponse page(PagedRequest page, CommoditySearchParams params);

    RemoteBaseResponse modify(CommodityDO remoteCommodity);

    void deleteStore(Long id);

    RemoteBaseResponse createTool(CreateRemoteCommodityRequest tool);

    PageRemoteCommodityResponse pageTools(PagedRequest page, CommoditySearchParams params);

    void deleteTool(Long id);
}
