package com.gergo.sb.dinnerboss.serviceapi.service.storage;

import java.util.ArrayList;
import java.util.List;

public class ListCommodityByStorageRemoteResponse {
    private Integer rc;


    private List<StorageContentDO> data ;

    public ListCommodityByStorageRemoteResponse() {
    }

    public ListCommodityByStorageRemoteResponse(Integer rc, List<StorageContentDO> data) {
        this.rc = rc;
        this.data = data;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public List<StorageContentDO> getData() {
        return data;
    }

    public void setData(List<StorageContentDO> data) {
        this.data = data;
    }
}
