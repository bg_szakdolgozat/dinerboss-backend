package com.gergo.sb.dinnerboss.serviceapi.service.customer;

public class ModifyRemoteCustomerRequest {
    private CustomerProfileDO customer;

    public ModifyRemoteCustomerRequest(CustomerProfileDO customer) {
        this.customer = customer;
    }

    public CustomerProfileDO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerProfileDO customer) {
        this.customer = customer;
    }
}
