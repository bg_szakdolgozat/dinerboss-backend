package com.gergo.sb.dinnerboss.serviceapi.service.storage;

import java.util.List;

public class GetAllByStoreRemoteResponse {

    private Integer rc;

    private List<StorageDO> stores = null;

    public GetAllByStoreRemoteResponse() {
    }

    public GetAllByStoreRemoteResponse(Integer rc, List<StorageDO> stores) {
        this.rc = rc;
        this.stores = stores;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public List<StorageDO> getStores() {
        return stores;
    }

    public void setStores(List<StorageDO> stores) {
        this.stores = stores;
    }
}
