package com.gergo.sb.dinnerboss.serviceapi.service.admin;

import java.math.BigInteger;
import java.util.Set;

public interface AdminDetails {
    Admin getAdmin();
    String getEmail();
    String getPassword();
    String getSessionId();

    static AdminDetails getInstance(Admin admin, String language, String password, String sessionId, String otpKey, Set<BigInteger> permittedOrgIds, BigInteger resellerId){
        return new AdminDetailsImpl(admin,  password, sessionId);
    }

    static AdminDetails preAuth(String email, String password) {
        Admin admin = new Admin();
        admin.setEmail(email);
        return new AdminDetailsImpl(admin, password, null);
    }

    static AdminDetails postAuth(String email, String sessionId) {
        Admin admin = new Admin();
        admin.setEmail(email);
        return new AdminDetailsImpl(admin, null,  sessionId);
    }
}
