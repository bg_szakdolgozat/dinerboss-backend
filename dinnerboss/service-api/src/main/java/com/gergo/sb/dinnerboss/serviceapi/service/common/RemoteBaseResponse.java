package com.gergo.sb.dinnerboss.serviceapi.service.common;

public class RemoteBaseResponse {
    private Integer rc;

    public RemoteBaseResponse(Integer rc) {
        this.rc = rc;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }
}
