package com.gergo.sb.dinnerboss.serviceapi.service.company;

public class GetRemoteCompanyByName {
    private Integer rc;

    private CompanyDO admin;

    public GetRemoteCompanyByName() {
    }

    public GetRemoteCompanyByName(Integer rc, CompanyDO admin) {
        this.rc = rc;
        this.admin = admin;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public CompanyDO getAdmin() {
        return admin;
    }

    public void setAdmin(CompanyDO admin) {
        this.admin = admin;
    }
}
