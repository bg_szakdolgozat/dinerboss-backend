package com.gergo.sb.dinnerboss.serviceapi.service.customer;

public class CreateRemoteCustomerResponse {
    private Integer rc;

    public CreateRemoteCustomerResponse(Integer rc) {
        this.rc = rc;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }
}
