package com.gergo.sb.dinnerboss.serviceapi.service.customer;

public class LoginCustomerResponseDO {
    private Integer rc;

    private CustomerProfileDO admin;

    public LoginCustomerResponseDO(Integer rc, CustomerProfileDO admin) {
        this.rc = rc;
        this.admin = admin;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public CustomerProfileDO getAdmin() {
        return admin;
    }

    public void setAdmin(CustomerProfileDO admin) {
        this.admin = admin;
    }
}
