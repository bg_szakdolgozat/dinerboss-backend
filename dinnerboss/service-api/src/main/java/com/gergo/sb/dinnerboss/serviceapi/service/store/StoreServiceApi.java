package com.gergo.sb.dinnerboss.serviceapi.service.store;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;

import java.util.List;

public interface StoreServiceApi {
    RemoteBaseResponse createStore(CreateRemoteStoreRequest store);

    StoreDO getStoreById(Long id);

    void deleteStore(Long id);

    RemoteBaseResponse modify(StoreDO remoteStore);

    PageRemoteStoreResponse page(PagedRequest page, StoreSearchParams params);

    GetRemoteStoresByCompIdResponse getStoresByCompId(Long id);


    RemoteBaseResponse assignUsers(Long storeId, List<Long> userIds);

    RemoteBaseResponse revokeUsers(Long storeId, List<Long> userIds);

    GetCustomersByIdResponse getByStoreId(Long storeId);
}
