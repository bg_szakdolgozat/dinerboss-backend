package com.gergo.sb.dinnerboss.serviceapi.service.admin;

public class CreateRequest {
    private CreateAdminDO admin;

    public CreateRequest(CreateAdminDO admin) {
        this.admin = admin;
    }

    public CreateAdminDO getAdmin() {
        return admin;
    }

    public void setAdmin(CreateAdminDO admin) {
        this.admin = admin;
    }
}
