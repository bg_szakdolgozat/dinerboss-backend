package com.gergo.sb.dinnerboss.serviceapi.service.admin;

public class GetRemoteAdminResponse {

    private Integer rc;
    private Admin admin;

    public GetRemoteAdminResponse(Integer rc, Admin admin) {
        this.rc = rc;
        this.admin = admin;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }
}
