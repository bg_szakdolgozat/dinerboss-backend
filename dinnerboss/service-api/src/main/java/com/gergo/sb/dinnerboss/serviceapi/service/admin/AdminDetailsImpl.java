package com.gergo.sb.dinnerboss.serviceapi.service.admin;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

public class AdminDetailsImpl implements AdminDetails {
    private Admin admin;
    private String password;
    private String sessionId;
    AdminDetailsImpl(Admin admin, String password, String sessionId) {
        this.admin = admin;
        this.password = password;
        this.sessionId = sessionId;
    }

    @Override
    public Admin getAdmin() {
        return admin;
    }



    @Override
    public String getEmail() {
        return admin.getEmail();
    }



    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getSessionId() {
        return sessionId;
    }



}