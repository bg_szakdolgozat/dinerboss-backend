package com.gergo.sb.dinnerboss.serviceapi.service.company;

import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;

import java.time.LocalDateTime;
import java.util.List;

public class CompanyDO {
    private Long id;
    private String name;
    private LocalDateTime founded;
    private String address;
    private List<CustomerProfileDO> owners ;

    public CompanyDO() {
    }

    public CompanyDO(Long id, String name, LocalDateTime founded, String address, List<CustomerProfileDO> owners) {
        this.id = id;
        this.name = name;
        this.founded = founded;
        this.address = address;
        this.owners = owners;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getFounded() {
        return founded;
    }

    public void setFounded(LocalDateTime founded) {
        this.founded = founded;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<CustomerProfileDO> getOwners() {
        return owners;
    }

    public void setOwners(List<CustomerProfileDO> owners) {
        this.owners = owners;
    }
}
