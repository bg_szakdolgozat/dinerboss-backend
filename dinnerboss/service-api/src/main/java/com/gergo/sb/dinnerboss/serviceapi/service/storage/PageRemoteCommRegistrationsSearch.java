package com.gergo.sb.dinnerboss.serviceapi.service.storage;

import java.util.List;

public class PageRemoteCommRegistrationsSearch {
    private String value;
    private List<Long> storageIds = null;
    private List<Long> commodityIds = null;
    private List<Long> customerIds = null;

    public PageRemoteCommRegistrationsSearch() {
    }

    public PageRemoteCommRegistrationsSearch(String value, List<Long> storageIds, List<Long> commodityIds, List<Long> customerIds) {
        this.value = value;
        this.storageIds = storageIds;
        this.commodityIds = commodityIds;
        this.customerIds = customerIds;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Long> getStorageIds() {
        return storageIds;
    }

    public void setStorageIds(List<Long> storageIds) {
        this.storageIds = storageIds;
    }

    public List<Long> getCommodityIds() {
        return commodityIds;
    }

    public void setCommodityIds(List<Long> commodityIds) {
        this.commodityIds = commodityIds;
    }

    public List<Long> getCustomerIds() {
        return customerIds;
    }

    public void setCustomerIds(List<Long> customerIds) {
        this.customerIds = customerIds;
    }
}
