package com.gergo.sb.dinnerboss.serviceapi.service.auth;

public class LoginRemoteAdminRequest {
    private String adminEmail;

    private String password;

    public LoginRemoteAdminRequest(String adminEmail, String password) {
        this.adminEmail = adminEmail;
        this.password = password;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
