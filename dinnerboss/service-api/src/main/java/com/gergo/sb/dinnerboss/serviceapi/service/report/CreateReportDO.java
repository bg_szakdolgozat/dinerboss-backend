package com.gergo.sb.dinnerboss.serviceapi.service.report;

public class CreateReportDO {

    private Long customerId;

    private Long storeId;

    private Long income;

    private Long expense;

    private String comment;

    private String date;

    public CreateReportDO() {
    }

    public CreateReportDO(Long customerId, Long storeId, Long income, Long expense, String comment, String date) {
        this.customerId = customerId;
        this.storeId = storeId;
        this.income = income;
        this.expense = expense;
        this.comment = comment;
        this.date = date;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public Long getIncome() {
        return income;
    }

    public void setIncome(Long income) {
        this.income = income;
    }

    public Long getExpense() {
        return expense;
    }

    public void setExpense(Long expense) {
        this.expense = expense;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
