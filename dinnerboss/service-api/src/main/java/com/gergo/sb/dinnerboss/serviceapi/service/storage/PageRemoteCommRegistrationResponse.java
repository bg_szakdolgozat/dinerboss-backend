package com.gergo.sb.dinnerboss.serviceapi.service.storage;

import com.gergo.sb.dinnerboss.serviceapi.service.commodity.CommodityDO;

import java.util.ArrayList;
import java.util.List;

public class PageRemoteCommRegistrationResponse {

    private Integer rc;
    private List<RemoteCommodityRegDO> data = new ArrayList<>();
    private Long totalCount;

    public PageRemoteCommRegistrationResponse() {
    }

    public PageRemoteCommRegistrationResponse(Integer rc, List<RemoteCommodityRegDO> data, Long totalCount) {
        this.rc = rc;
        this.data = data;
        this.totalCount = totalCount;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public List<RemoteCommodityRegDO> getData() {
        return data;
    }

    public void setData(List<RemoteCommodityRegDO> data) {
        this.data = data;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
