package com.gergo.sb.dinnerboss.serviceapi.service.company;

public class ModifyRemoteCompanyRequest {
    private CompanyDO company;

    public ModifyRemoteCompanyRequest() {
    }

    public ModifyRemoteCompanyRequest(CompanyDO company) {
        this.company = company;
    }

    public CompanyDO getCompany() {
        return company;
    }

    public void setCompany(CompanyDO company) {
        this.company = company;
    }
}
