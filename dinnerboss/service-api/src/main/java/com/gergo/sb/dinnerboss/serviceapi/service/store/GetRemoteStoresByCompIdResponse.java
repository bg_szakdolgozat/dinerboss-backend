package com.gergo.sb.dinnerboss.serviceapi.service.store;

import java.util.List;

public class GetRemoteStoresByCompIdResponse {
    private Integer rc;

    private List<StoreDO> store ;

    public GetRemoteStoresByCompIdResponse() {
    }

    public GetRemoteStoresByCompIdResponse(Integer rc, List<StoreDO> store) {
        this.rc = rc;
        this.store = store;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public List<StoreDO> getStore() {
        return store;
    }

    public void setStore(List<StoreDO> store) {
        this.store = store;
    }
}
