package com.gergo.sb.dinnerboss.serviceapi.service.store;

import com.gergo.sb.dinnerboss.serviceapi.service.company.CompanyDO;

import java.time.LocalDateTime;

public class StoreDO {
    private Long id;

    private String name;

    private LocalDateTime opened;

    private String location;

    private String phoneNumber;

    private CompanyDO company;

    public StoreDO() {
    }

    public StoreDO(Long id, String name, LocalDateTime opened, String location, String phoneNumber, CompanyDO company) {
        this.id = id;
        this.name = name;
        this.opened = opened;
        this.location = location;
        this.phoneNumber = phoneNumber;
        this.company = company;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getOpened() {
        return opened;
    }

    public void setOpened(LocalDateTime opened) {
        this.opened = opened;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public CompanyDO getCompany() {
        return company;
    }

    public void setCompany(CompanyDO company) {
        this.company = company;
    }
}
