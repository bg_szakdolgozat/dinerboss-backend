package com.gergo.sb.dinnerboss.serviceapi.service.commodity;

import com.gergo.sb.dinnerboss.serviceapi.service.customer.CreateCustomerDO;

public class CreateRemoteCommodityRequest {

    private CreateCommodityDO commodity;

    public CreateRemoteCommodityRequest(CreateCommodityDO commodity) {
        this.commodity = commodity;
    }

    public CreateCommodityDO getCommodity() {
        return commodity;
    }

    public void setCommodity(CreateCommodityDO commodity) {
        this.commodity = commodity;
    }
}
