package com.gergo.sb.dinnerboss.serviceapi.service.admin;




public interface AdminServiceApi {
    CreateRemoteAdminResponse createAdmin(CreateRequest request);
    GetRemoteAdminResponse getAdmin(String email);
}
