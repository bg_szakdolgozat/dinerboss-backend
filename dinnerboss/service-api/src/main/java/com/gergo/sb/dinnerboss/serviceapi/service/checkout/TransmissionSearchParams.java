package com.gergo.sb.dinnerboss.serviceapi.service.checkout;

import java.util.List;

public class TransmissionSearchParams {
    private String value;

    private List<Long> checkoutIds = null;

    private List<Long> customerIds = null;

    public TransmissionSearchParams() {
    }

    public TransmissionSearchParams(String value, List<Long> checkoutIds, List<Long> customerIds) {
        this.value = value;
        this.checkoutIds = checkoutIds;
        this.customerIds = customerIds;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Long> getCheckoutIds() {
        return checkoutIds;
    }

    public void setCheckoutIds(List<Long> checkoutIds) {
        this.checkoutIds = checkoutIds;
    }

    public List<Long> getCustomerIds() {
        return customerIds;
    }

    public void setCustomerIds(List<Long> customerIds) {
        this.customerIds = customerIds;
    }
}
