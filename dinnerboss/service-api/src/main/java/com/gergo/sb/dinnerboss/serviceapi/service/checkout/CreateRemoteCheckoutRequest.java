package com.gergo.sb.dinnerboss.serviceapi.service.checkout;

public class CreateRemoteCheckoutRequest {

    private CreateCheckoutDO createCheckoutDO;

    public CreateRemoteCheckoutRequest() {
    }

    public CreateRemoteCheckoutRequest(CreateCheckoutDO checkoutDO) {
        this.createCheckoutDO = checkoutDO;
    }

    public CreateCheckoutDO getCreateCheckoutDO() {
        return createCheckoutDO;
    }

    public void setCreateCheckoutDO(CreateCheckoutDO createCheckoutDO) {
        this.createCheckoutDO = createCheckoutDO;
    }
}
