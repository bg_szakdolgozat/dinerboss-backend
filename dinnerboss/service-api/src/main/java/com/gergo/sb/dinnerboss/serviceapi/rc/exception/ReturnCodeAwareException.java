package com.gergo.sb.dinnerboss.serviceapi.rc.exception;

public abstract class ReturnCodeAwareException extends RuntimeException{
    public ReturnCodeAwareException() {
    }

    public ReturnCodeAwareException(String message) {
        super(message);
    }

    public ReturnCodeAwareException(Throwable cause) {
        super(cause);
    }

    public ReturnCodeAwareException(String message, Throwable cause) {
        super(message, cause);
    }

    public abstract int getRc();

}
