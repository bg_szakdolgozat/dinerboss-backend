package com.gergo.sb.dinnerboss.serviceapi.service.checkout;

import java.util.List;

public class CheckoutSearchParams {

    private String value;

    private String name;

    private List<Long> checkoutIds = null;

    private List<Long> storeIds = null;

    public CheckoutSearchParams() {
    }

    public CheckoutSearchParams(String value, String name, List<Long> checkoutIds, List<Long> storeIds) {
        this.value = value;
        this.name = name;
        this.checkoutIds = checkoutIds;
        this.storeIds = storeIds;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getCheckoutIds() {
        return checkoutIds;
    }

    public void setCheckoutIds(List<Long> checkoutIds) {
        this.checkoutIds = checkoutIds;
    }

    public List<Long> getStoreIds() {
        return storeIds;
    }

    public void setStoreIds(List<Long> storeIds) {
        this.storeIds = storeIds;
    }
}
