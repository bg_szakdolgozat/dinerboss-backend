package com.gergo.sb.dinnerboss.serviceapi.service.common;

import java.util.List;

public class PagedData<T> {
    private List<T> data;
    private Long totalCount;

    public PagedData(List<T> data, Long totalCount) {
        this.data = data;
        this.totalCount = totalCount;
    }

    public List<T> getData() {
        return data;
    }

    public Long getTotalCount() {
        return totalCount;
    }
}

