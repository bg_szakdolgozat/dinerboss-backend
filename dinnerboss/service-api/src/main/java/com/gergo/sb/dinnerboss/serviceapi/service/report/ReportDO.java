package com.gergo.sb.dinnerboss.serviceapi.service.report;

import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;
import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreDO;

public class ReportDO {

    private Long id;

    private CustomerProfileDO customer;

    private StoreDO store;

    private Long income;

    private Long expense;

    private String comment;

    private String date;

    public ReportDO() {
    }

    public ReportDO(Long id, CustomerProfileDO customer, StoreDO store, Long income, Long expense, String comment, String date) {
        this.id = id;
        this.customer = customer;
        this.store = store;
        this.income = income;
        this.expense = expense;
        this.comment = comment;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CustomerProfileDO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerProfileDO customer) {
        this.customer = customer;
    }

    public StoreDO getStore() {
        return store;
    }

    public void setStore(StoreDO store) {
        this.store = store;
    }

    public Long getIncome() {
        return income;
    }

    public void setIncome(Long income) {
        this.income = income;
    }

    public Long getExpense() {
        return expense;
    }

    public void setExpense(Long expense) {
        this.expense = expense;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
