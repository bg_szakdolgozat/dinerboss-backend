package com.gergo.sb.dinnerboss.serviceapi.service.admin;


public class CreateRemoteAdminResponse {
    private Integer rc;

    public CreateRemoteAdminResponse(Integer rc) {
        this.rc = rc;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

}
