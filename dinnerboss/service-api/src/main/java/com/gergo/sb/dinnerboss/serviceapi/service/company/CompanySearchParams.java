package com.gergo.sb.dinnerboss.serviceapi.service.company;

import java.util.List;

public class CompanySearchParams {

    private String value;
    private String name;
    private List<Long> compIds = null;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getCompIds() {
        return compIds;
    }

    public void setCompIds(List<Long> compIds) {
        this.compIds = compIds;
    }
}
