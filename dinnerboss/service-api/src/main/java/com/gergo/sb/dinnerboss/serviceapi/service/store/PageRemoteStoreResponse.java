package com.gergo.sb.dinnerboss.serviceapi.service.store;

import java.util.ArrayList;
import java.util.List;

public class PageRemoteStoreResponse {
    private Integer rc;
    private List<StoreDO> data = new ArrayList<>();
    private Long totalCount;

    public PageRemoteStoreResponse(Integer rc, List<StoreDO> data, Long totalCount) {
        this.rc = rc;
        this.data = data;
        this.totalCount = totalCount;
    }

    public Integer getRc() {
        return rc;
    }

    public void setRc(Integer rc) {
        this.rc = rc;
    }

    public List<StoreDO> getData() {
        return data;
    }

    public void setData(List<StoreDO> data) {
        this.data = data;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
