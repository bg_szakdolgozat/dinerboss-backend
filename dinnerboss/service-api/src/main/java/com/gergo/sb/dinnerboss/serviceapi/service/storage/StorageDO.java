package com.gergo.sb.dinnerboss.serviceapi.service.storage;

import com.gergo.sb.dinnerboss.serviceapi.service.store.StoreDO;

public class StorageDO {
    private Long id;

    private StoreDO store;

    public StorageDO() {
    }

    public StorageDO(Long id, StoreDO store) {
        this.id = id;
        this.store = store;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StoreDO getStore() {
        return store;
    }

    public void setStore(StoreDO store) {
        this.store = store;
    }
}
