package com.gergo.sb.dinnerboss.serviceapi.service.customer;

public class CreateRemoteCustomerRequest {

    private CreateCustomerDO customer;

    public CreateRemoteCustomerRequest(CreateCustomerDO customer) {
        this.customer = customer;
    }

    public CreateCustomerDO getCustomer() {
        return customer;
    }

    public void setCustomer(CreateCustomerDO customer) {
        this.customer = customer;
    }
}
