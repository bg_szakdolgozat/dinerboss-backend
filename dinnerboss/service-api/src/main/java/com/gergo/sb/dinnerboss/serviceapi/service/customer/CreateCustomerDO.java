package com.gergo.sb.dinnerboss.serviceapi.service.customer;

import java.math.BigInteger;
import java.util.List;

public class CreateCustomerDO {
    private String title;

    private String firstName;

    private String lastName;

    private Long status;

    private String phoneNumber;

    private String password;

    private String email;

    private List<Long> compIds;

    public CreateCustomerDO() {
    }

    public CreateCustomerDO(String title, String firstName, String lastName, Long status, String phoneNumber, String password, String email) {
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Long> getCompIds() {
        return compIds;
    }

    public void setCompIds(List<Long> compIds) {
        this.compIds = compIds;
    }
}
