package com.gergo.sb.dinnerboss.serviceapi.rc;

public enum ReturnCode {
    OK(0),
    USER_ALREADY_EXIST(200);
    private int code;

    ReturnCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}

