package com.gergo.sb.dinnerboss.serviceapi.service.checkout;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;

public interface CheckoutServiceApi {
    RemoteBaseResponse createCommodity(CreateRemoteCheckoutRequest checkout);

    PageRemoteCheckoutResponse page(PagedRequest page, CheckoutSearchParams params);

    void deleteCheckout(Long id);

    RemoteBaseResponse modify(ModifyCheckoutDO remoteCheckout);

    RemoteBaseResponse addTransmission(AddTransmissionDO checkout);

    PageRemoteTransmissionResponse pageTransmissions(PagedRequest page, TransmissionSearchParams params);
}
