package com.gergo.sb.dinnerboss.serviceapi.service.company;

import com.gergo.sb.dinnerboss.serviceapi.service.common.PagedRequest;
import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteBaseResponse;
import com.gergo.sb.dinnerboss.serviceapi.service.customer.CustomerProfileDO;

import java.time.LocalDateTime;
import java.util.List;

public interface CompanyServiceApi {

    RemoteBaseResponse createCompany(CreateRemoteCompanyRequest admin);

    GetRemoteCompanyByName getCompanyByName(String name);

    RemoteBaseResponse modify(ModifyRemoteCompanyRequest company);

    PageRemoteCompaniesResponse page(PagedRequest page, CompanySearchParams params);

    RemoteBaseResponse removeOwner(Long companyId, Long customerId);

    void deleteCompany(Long id);

    RemoteBaseResponse assignOwners(Long companyId, List<Long> customerIds);

    GetOwnersByIdResponse getByCompanyId(Long compId);

    StoreIncomeResponse getStoreIncomes(List<Long> storeIds, Long days);

    StoreIncomeResponse getStoreReports(Long storeId, Long days);
}
