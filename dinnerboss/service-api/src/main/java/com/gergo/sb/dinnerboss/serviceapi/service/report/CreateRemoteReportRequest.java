package com.gergo.sb.dinnerboss.serviceapi.service.report;

public class CreateRemoteReportRequest {
    private CreateReportDO report;

    public CreateRemoteReportRequest() {
    }

    public CreateRemoteReportRequest(CreateReportDO report) {
        this.report = report;
    }

    public CreateReportDO getReport() {
        return report;
    }

    public void setReport(CreateReportDO report) {
        this.report = report;
    }
}
