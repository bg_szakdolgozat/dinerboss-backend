package com.gergo.sb.dinnerboss.serviceapi.service.company;

public class CreateRemoteCompanyRequest {
    private CreateCompanyDO company;

    public CreateRemoteCompanyRequest(CreateCompanyDO company) {
        this.company = company;
    }

    public CreateCompanyDO getCompany() {
        return company;
    }

    public void setCompany(CreateCompanyDO company) {
        this.company = company;
    }
}
