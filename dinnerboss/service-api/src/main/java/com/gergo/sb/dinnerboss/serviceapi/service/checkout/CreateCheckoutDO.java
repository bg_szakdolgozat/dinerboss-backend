package com.gergo.sb.dinnerboss.serviceapi.service.checkout;

public class CreateCheckoutDO {

    private String name;
    private String comment;
    private Long storeId;

    public CreateCheckoutDO() {
    }

    public CreateCheckoutDO(String name, String comment, Long storeId) {
        this.name = name;
        this.comment = comment;
        this.storeId = storeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }
}
