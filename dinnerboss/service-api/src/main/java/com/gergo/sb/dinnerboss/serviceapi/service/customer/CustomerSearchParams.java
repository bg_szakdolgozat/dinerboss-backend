package com.gergo.sb.dinnerboss.serviceapi.service.customer;

import com.gergo.sb.dinnerboss.serviceapi.service.common.RemoteSearchParams;

import java.util.List;

public class CustomerSearchParams extends RemoteSearchParams {
    private String firstName;
    private String lastName;
    private String email;
    private List<Long> customerIds = null;
    private List<Long> storeIds = null;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Long> getCustomerIds() {
        return customerIds;
    }

    public void setCustomerIds(List<Long> customerIds) {
        this.customerIds = customerIds;
    }

    public List<Long> getStoreIds() {
        return storeIds;
    }

    public void setStoreIds(List<Long> storeIds) {
        this.storeIds = storeIds;
    }
}
