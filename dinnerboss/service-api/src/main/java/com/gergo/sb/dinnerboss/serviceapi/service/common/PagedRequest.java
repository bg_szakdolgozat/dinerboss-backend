package com.gergo.sb.dinnerboss.serviceapi.service.common;

public class PagedRequest {
    private Integer pageNumber;
    private Integer pageSize;
    private String orderBy;
    private String direction;
    private RemoteSearchParams searchParams;

    public PagedRequest() {
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public RemoteSearchParams getSearchParams() {
        return searchParams;
    }

    public PagedRequest withSearchParams(RemoteSearchParams searchParams) {
        this.searchParams = searchParams;
        return this;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public void setSearchParams(RemoteSearchParams searchParams) {
        this.searchParams = searchParams;
    }
}
